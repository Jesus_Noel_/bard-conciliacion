﻿Imports System.Data
Imports System.Data.OleDb

Public Class frm_passmto
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
        frm_mto.Show()
    End Sub

    Private Sub frm_passmto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim i As Integer

        For i = 0 To 99
            If usuarios(i).user = "" Then Exit For
            ListBox1.Items.Add(usuarios(i).user)
        Next
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        Dim indice As Integer
        TextBox1.Text = ListBox1.SelectedItem
        indice = ListBox1.SelectedIndex
        'TextBox2.Text = usuarios(indice).pass
        'TextBox3.Text = usuarios(indice).level
        TextBox4.Text = usuarios(indice).reloj
        TextBox5.Text = usuarios(indice).fechaini
        TextBox6.Text = usuarios(indice).fechafin

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim consulta As String
        Dim icount As Integer
        Dim cmd As New OleDbCommand
        Dim i As Integer
        Dim fech1 As Date
        Dim fech2 As Date

        Dim result As Integer = MessageBox.Show("Seguro de INSERTAR record?", "INSERTAR", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If TextBox1.Text <> "" And TextBox2.Text <> "" And TextBox3.Text <> "" And TextBox4.Text <> "" And TextBox5.Text <> "" And TextBox6.Text <> "" Then

                fech1 = FormatDateTime(TextBox5.Text, DateFormat.ShortDate)
                fech2 = DateAdd("m", 1, fech1)
                TextBox6.Text = FormatDateTime((fech2).ToString, DateFormat.ShortDate)

                BD_AbrirConexion()
                consulta = "INSERT INTO tblUsuarios (Nombre,clave,nivel,Numero_reloj,Fecha_Inicial,Fecha_Final) VALUES ('" & TextBox1.Text & "','" & TextBox2.Text & _
                    "','" & TextBox3.Text & "','" & TextBox4.Text & "','" & TextBox5.Text & "','" & TextBox6.Text & "')"
                cmd = New OleDbCommand(consulta, CNN)
                icount = cmd.ExecuteNonQuery
                'conexion.Close()
                System.Threading.Thread.Sleep(500)
                'conexion.Open()
                consulta = "SELECT * From tblUsuarios "
                cmd = New OleDbCommand(consulta, CNN)

                dr = cmd.ExecuteReader
                i = 0
                While dr.Read()
                    usuarios(i).user = dr("Nombre").ToString
                    usuarios(i).pass = dr("clave").ToString
                    usuarios(i).level = dr("nivel").ToString
                    usuarios(i).reloj = dr("Numero_reloj").ToString
                    usuarios(i).fechaini = dr("Fecha_Inicial").ToString
                    usuarios(i).fechafin = dr("Fecha_Final").ToString
                    i = i + 1
                End While
                ListBox1.Items.Clear()  ''limpiamos lista y la reconstruimos
                For i = 0 To 99
                    If usuarios(i).user = "" Then Exit For
                    ListBox1.Items.Add(usuarios(i).user)
                Next
                CNN.Close()
            Else
                MessageBox.Show("VALORES MAL")
            End If
        ElseIf result = DialogResult.No Then
            MessageBox.Show("OPERACION CANCELADA")
        End If
        inicializa_base()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim consulta As String
        Dim icount As Integer
        Dim cmd As New OleDbCommand
        Dim i As String
        Dim fech1 As Date
        Dim fech2 As Date

        Dim result As Integer = MessageBox.Show("Seguro de MODIFICAR record?", "MODIFICAR", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If TextBox1.Text <> "" And TextBox2.Text <> "" And TextBox3.Text <> "" And TextBox4.Text <> "" And TextBox5.Text <> "" And TextBox6.Text <> "" Then
                CNN.Open()
                fech1 = FormatDateTime(TextBox5.Text, DateFormat.ShortDate)
                fech2 = DateAdd("m", 1, fech1)
                TextBox6.Text = FormatDateTime((fech2).ToString, DateFormat.ShortDate)
                consulta = "UPDATE tblUsuarios SET clave='" & TextBox2.Text & "', nivel='" & TextBox3.Text & "', Numero_reloj='" & TextBox4.Text & _
                    "', Fecha_Inicial='" & fech1 & "', Fecha_Final='" & fech2 & "' WHERE Nombre='" & TextBox1.Text & "'"
                cmd = New OleDbCommand(consulta, CNN)
                icount = cmd.ExecuteNonQuery
                consulta = "SELECT * From tblUsuarios "
                cmd = New OleDbCommand(consulta, CNN)

                dr = cmd.ExecuteReader
                i = 0
                While dr.Read()
                    usuarios(i).user = dr("nombre").ToString
                    usuarios(i).pass = dr("clave").ToString
                    usuarios(i).level = dr("nivel").ToString
                    usuarios(i).reloj = dr("Numero_reloj").ToString
                    usuarios(i).fechaini = dr("Fecha_Inicial").ToString
                    usuarios(i).fechafin = dr("Fecha_Final").ToString
                    i = i + 1
                End While
                CNN.Close()
            Else
                MessageBox.Show("VALORES MAL")
            End If
        ElseIf result = DialogResult.No Then
            MessageBox.Show("OPERACION CANCELADA")
        End If



    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim consulta As String
        Dim icount As Integer
        Dim cmd As New OleDbCommand
        Dim i As Integer

        Dim result As Integer = MessageBox.Show("Seguro de BORRAR record?", "BORRAR", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            CNN.Open()
            consulta = "DELETE from tblUsuarios WHERE Nombre='" & TextBox1.Text & "'"
            cmd = New OleDbCommand(consulta, CNN)
            icount = cmd.ExecuteNonQuery
            CNN.Close()
            TextBox1.Text = ""
            TextBox2.Text = ""
            TextBox3.Text = ""
            TextBox4.Text = ""
            TextBox5.Text = ""
            TextBox6.Text = ""
            System.Threading.Thread.Sleep(500)
            CNN.Open()
            consulta = "SELECT * From tblUsuarios "
            cmd = New OleDbCommand(consulta, CNN)

            dr = cmd.ExecuteReader
            i = 0
            For i = 0 To 99
                usuarios(i).user = ""
                usuarios(i).pass = ""
                usuarios(i).level = 0
            Next
            i = 0
            While dr.Read()
                usuarios(i).user = dr("nombre").ToString
                usuarios(i).pass = dr("clave").ToString
                usuarios(i).level = dr("nivel").ToString
                usuarios(i).reloj = dr("Numero_reloj").ToString
                usuarios(i).fechaini = dr("Fecha_Inicial").ToString
                usuarios(i).fechafin = dr("Fecha_Final").ToString
                i = i + 1
            End While
            ListBox1.Items.Clear()  ''limpiamos lista y la reconstruimos
            For i = 0 To 99
                If usuarios(i).user = "" Then Exit For
                ListBox1.Items.Add(usuarios(i).user)
            Next
            CNN.Close()
        ElseIf result = DialogResult.No Then
            MessageBox.Show("OPERACION CANCELADA")
        End If
    End Sub



    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged
        Dim fech2 As Date
        Dim fech1 As Date

        TextBox5.Text = Date.Today

        fech1 = FormatDateTime(TextBox5.Text, DateFormat.ShortDate)
        fech2 = DateAdd("m", 1, fech1)
        TextBox6.Text = FormatDateTime((fech2).ToString, DateFormat.ShortDate)
    End Sub
End Class