﻿Imports System.Threading
Public Class frm_password
    Dim indice As Integer
    Private Sub TextBoxNumReloj_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBoxNumReloj.LostFocus
        If TextBoxNumReloj.Text <> "" Then
            inicializa_base()
            LabelUsuario.Text = usuarios(indice).user
        Else
            MsgBox("USUARIO NO ENCONTRDO", MsgBoxStyle.Information, "ERROR")
        End If
    End Sub
    Private Sub frm_password_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim i As Integer
        inicializa_base()
        For i = 0 To 99
            If usuarios(i).user = "" Then Exit For
            ListBoxUsurios.Items.Add(usuarios(i).user)
        Next

        TextBoxNumReloj.Text = ""
        TextBoxClave.Text = ""
        LabelUsuario.Text = ""
        Label4.Text = ""

    End Sub
    Private Sub ListBoxUsurios_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBoxUsurios.SelectedIndexChanged
        ' Dim indice As Integer
        '  TextBoxNumReloj.Text = ListBoxUsurios.SelectedItem
        indice = ListBoxUsurios.SelectedIndex
        TextBoxNumReloj.Text = usuarios(indice).reloj
        LabelUsuario.Text = usuarios(indice).user
    End Sub
    Private Sub ButtonAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAceptar.Click
        If LabelUsuario.Text = "" Then
            Label4.BackColor = Color.Orange
            Label4.Text = "ASEGURESE DE QUE ESTA DADO DE ALTA EN EL SISTEMA"
        Else
            If mantennimiento = True Then
                If bypass = False Then
                    passOK = False
                    If TextBoxClave.Text = "" Then
                        Label4.BackColor = Color.Orange
                        Label4.Text = "TECLEE SU CLAVE"
                    Else
                        If (nivel <= usuarios(indice).level) And (LabelUsuario.Text <> "") Then
                            If (TextBoxClave.Text = usuarios(indice).pass) Then
                                passOK = True
                                Label4.BackColor = Color.LightGreen
                                Label4.ForeColor = Color.Blue
                                Label4.Text = "CLAVE OK"
                                Label4.Update()
                                nombreusuario = LabelUsuario.Text
                                Thread.Sleep(500)
                                Me.Close()
                                frm_mto.Show()
                            Else
                                passOK = False
                                Label4.BackColor = Color.Red
                                Label4.Text = "CLAVE EQUIVOCADA"
                                Label4.ForeColor = Color.Black
                            End If
                        Else
                            passOK = False
                            Label4.BackColor = Color.Red
                            Label4.Text = "NO TIENE PRIVILEGIO"
                        End If
                    End If
                Else
                    passOK = True
                    NOMBRE_USUARIO = LabelUsuario.Text
                    Thread.Sleep(500)
                    Me.Close()
                    frm_mto.Show()
                End If
                'mantennimiento = False
            ElseIf conciliando = True Then
                If bypass = False Then
                    passOK = False
                    If TextBoxClave.Text = "" Then
                        Label4.BackColor = Color.Orange
                        Label4.Text = "TECLEE SU CLAVE"
                    Else
                        If (nivel <= usuarios(indice).level) And (LabelUsuario.Text <> "") Then
                            If (TextBoxClave.Text = usuarios(indice).pass) Then
                                passOK = True
                                Label4.BackColor = Color.LightGreen
                                Label4.ForeColor = Color.Blue
                                Label4.Text = "CLAVE OK"
                                Label4.Update()
                                nombreusuario = LabelUsuario.Text
                                Thread.Sleep(500)
                                Me.Close()
                                frm_prod.Show()
                            Else
                                passOK = False
                                Label4.BackColor = Color.Red
                                Label4.Text = "CLAVE EQUIVOCADA"
                                Label4.ForeColor = Color.Black
                            End If
                        Else
                            passOK = False
                            Label4.BackColor = Color.Red
                            Label4.Text = "NO TIENE PRIVILEGIO"
                        End If
                    End If
                Else
                    passOK = True
                    NOMBRE_USUARIO = LabelUsuario.Text
                    Thread.Sleep(500)
                    Me.Close()
                    frm_prod.Show()
                End If
            Else
                If bypass = False Then
                    passOK = False
                    If TextBoxClave.Text = "" Then
                        Label4.BackColor = Color.Orange
                        Label4.Text = "TECLEE SU CLAVE"
                    Else
                        If (nivel <= usuarios(indice).level) And (LabelUsuario.Text <> "") Then
                            If (TextBoxClave.Text = usuarios(indice).pass) Then
                                passOK = True
                                Label4.BackColor = Color.LightGreen
                                Label4.ForeColor = Color.Blue
                                Label4.Text = "CLAVE OK"
                                Label4.Update()
                                nombreusuario = LabelUsuario.Text
                                Thread.Sleep(500)
                                Me.Close()
                                frm_prod.Show()
                                passOK = True

                            Else
                                passOK = False
                                Label4.BackColor = Color.Red
                                Label4.Text = "CLAVE EQUIVOCADA"
                                Label4.ForeColor = Color.Black
                            End If
                        Else
                            passOK = False
                            Label4.BackColor = Color.Red
                            Label4.Text = "NO TIENE PRIVILEGIO"
                        End If
                    End If
                Else
                    passOK = True
                    NOMBRE_USUARIO = LabelUsuario.Text
                    Thread.Sleep(500)
                    Me.Close()
                    frm_prod.Show()
                End If
            End If
            End If
    End Sub

    Private Sub ButtonRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRegresar.Click
        '  If mantennimiento = True Then
        'Me.Hide()
        ' frm_mto.Show()
        '  Else
        If conciliando = True Then
            passOK = False
            Me.Hide()


        Else
            passOK = False
            Me.Hide()
            frm_ppal.Show()
        End If
        
        '  End If

    End Sub
End Class