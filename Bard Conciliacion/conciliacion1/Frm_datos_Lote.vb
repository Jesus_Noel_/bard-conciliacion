﻿Imports System.Math
Public Class Frm_datos_Lote
    Dim a As Integer
    Dim b As Integer
    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Chr(13) = e.KeyChar Then
            TextBox1.CharacterCasing = CharacterCasing.Upper
            If Len(TextBox1.Text) > 7 Then
                'Dim int As Integer
                ' '' primero vemos que no este ya en la lista de lotes n base access y su situacion
                IDLote = TextBox1.Text
                loteencontrado = False
                loteenCSV = False = False
                buscalote()     ''buscamos en access primero para ver si esta suspendido o ya paso
                If loteencontrado = False Then  ''Si no existe hay que sacar la info del archivo csv
                    DATOS_DE_LOTE() 'leerCSV()       ''buscamos en CSV para ver si existe
                    If loteenCSV = True Then  ''Si  existe hay que sacar la info del archivo datos en access
                        Label15.Text = "LOTE NUEVO, LLENE LA INFORMACION"
                        lotenuevo = True
                        TextBox4.Enabled = True
                        'TextBox5.Enabled = True
                        TextBox6.Enabled = True
                        TextBox3.Enabled = True
                        TextBox6.Focus()
                    Else
                        MsgBox("LOTE NO ESTA NI PROCESADO NI LISTO PARA ELLO")
                        TextBox1.Text = ""
                        TextBox1.Focus()
                        lotenuevo = False
                    End If
                Else
                    If statuslote = 2 Or statuslote = 1 Then
                        If statuslote = 2 Then Label15.Text = "LOTE ESTABA SUSPENDIDO"
                        If statuslote = 1 Then Label15.Text = "LOTE ESTABA INTERRUMPIDO"
                        leerloteconciliando()

                        TextBox2.ReadOnly = True
                        TextBox3.ReadOnly = True
                        TextBox6.ReadOnly = True
                        TextBox5.ReadOnly = True
                        TextBox4.ReadOnly = True
                        TextBox7.ReadOnly = True
                        TextBox8.ReadOnly = True
                        TextBox2.BackColor = Color.White
                        TextBox3.BackColor = Color.White
                        TextBox6.BackColor = Color.White
                        TextBox5.BackColor = Color.White
                        TextBox4.BackColor = Color.White
                        TextBox7.BackColor = Color.White
                        TextBox8.BackColor = Color.White
                        TextBox2.Text = Modelo
                        TextBox3.Text = Meta
                        TextBox6.Text = ExactCajasInternas
                        TextBox5.Text = ExactCajasExternas
                        TextBox4.Text = PIEZAS_POR_CAJA
                        TextBox7.Text = ActCajasInternas
                        TextBox8.Text = ActCajasExternas

                        ButtonAceptar.Focus()

                        lotenuevo = False

                    End If
                    If statuslote = 3 Then
                        Label15.Text = "LOTE TERMINADO"
                        'DATOS_DE_LOTE()
                        'Me.Close()
                        'frm_prod.Show()
                        TextBox2.Text = ""
                        TextBox4.Text = ""
                        TextBox5.Text = ""
                        TextBox6.Text = ""
                        TextBox5.Text = ""
                        TextBox1.Text = ""
                        TextBox1.Focus()
                    End If
                End If

            End If

        End If

    End Sub
    Private Sub TextBox6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox6.KeyPress
        'If Chr(13) = e.KeyChar Then
        '    If Len(TextBox1.Text) > 2 Then
        '        TextBox5.Focus()
        '    End If
        'End If


        'If Chr(13) = e.KeyChar Then
        '    If Len(TextBox1.Text) > 2 Then
        '        ButtonAceptar.Focus()
        '    End If
        'End If
        'If TextBox6.Text.Length > 0 Then
        '    Dim div As Integer = TextBox3.Text / TextBox6.Text

        '    TextBox4.Text = div
        'End If
       

    End Sub

    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        'If Chr(13) = e.KeyChar Then
        '    If Len(TextBox1.Text) > 2 Then
        '        ButtonAceptar.Focus()
        '    End If
        'End If

        ''a = Math.Truncate(Val(TextBox3.Text) / Val(TextBox6.Text)) + 1
        'TextBox4.Text = 5
    End Sub
    Private Sub ButtonAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAceptar.Click
        'b = (Val(TextBox3.Text) / Val(TextBox5.Text)) + 1
        IDMODELO = TextBox2.Text
        ' ExactCajasInternas = TextBox6.Text
        ExactCajasInternas = TextBox3.Text / TextBox4.Text
        PIEZAS_POR_CAJA = TextBox4.Text 'piezas por caja inner
        '  cajasinternasXexterna = TextBox3.Text / TextBox4.Text 'piezas por caja externa ' Numero de cajas inner por caja outer (inner/outter)
        cajasinternasXexterna = TextBox6.Text 'piezas por caja externa ' Numero de cajas inner por caja outer (inner/outter)

        ExactCajasExternas = TextBox5.Text
        PIEZAS_TOTALES_LOTES = TextBox3.Text
        Meta = TextBox3.Text
        lote_PROD = TextBox1.Text
        ActCajasInternas = Val(TextBox7.Text)
        ActCajasExternas = Val(TextBox8.Text)
        CAJAS_INNER_POR_OUTTER = 2 'PIEZAS_TOTALES_LOTES / (PIEZAS_POR_CAJA * ExactCajasExternas)
        If RadioButton1.Checked = True Then
            Num_Codigos = 1
        Else
            Num_Codigos = 2
        End If
        Me.Close()
        frm_prod.Show()
    End Sub

    Private Sub ButtonRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRegresar.Click
        Me.Close()
        frm_ppal.Show()
    End Sub




    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        Dim multi As Integer
        Dim div As Integer

        If Chr(13) = e.KeyChar Then
            If IsNumeric(TextBox4.Text) = True And IsNumeric(TextBox6.Text) = True Then


                If Len(TextBox1.Text) > 2 Then
                    ButtonAceptar.Focus()
                End If
                multi = TextBox4.Text * TextBox6.Text

                div = TextBox3.Text / multi

                TextBox5.Text = div
            Else

                MsgBox("Cantidades Invalidas")
                TextBox4.Text = ""
                TextBox6.Text = ""
                TextBox6.Focus()
            End If
       
        End If
    End Sub
  
  
    
  
    
   
    Private Sub Frm_datos_Lote_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class