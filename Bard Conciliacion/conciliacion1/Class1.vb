﻿Public Class Class1
    'Se estan usando 
    Public Property UsuarioInicial_REPORTE As String
    Public Property Estacion_REPORTE As String
    Public Property Num_Emple_inicio_REPORTE As String
    Public Property Fecha_Ini_REPORTE As String
    Public Property Lote_REPORTE As String
    Public Property Modelo_reporte As String
    Public Property PKS_REPORTES As String
    Public Property ExactCajasInternas_REPORTE As String
    Public Property ExactCajasExternas_REPORTE As String
    Public Property ActCajasInternas_REPORTE As Integer
    Public Property ActCajasExternasREPORTE As String
    Public Property Scrap_Interno_REPORTE As Integer
    Public Property Scrap_Externo_REPORTE As String


    Public Property Meta_REPORTE As String ' Se usa para el Total de inners
    Public Property Status_REPORTE As String ' Se usa para Total de outers

    'Libres 
    Public Property Nombre_REPORTE As String
    Public Property Num_Emple_Termi_REPORTE As String
    Public Property Fecha_Fin_REPORTE As String



    Public Property Scrap_Externo3D_REPORTE As String
    Public Property Conciliado_REPORTE As String

    Public Property UsuarioConciliado_REPORTE As String
    Public Property Comentario_REPORTE As String


    'Nuevos
    Public Property ExactPiezasTotales_REPORTE As String
    Public Property ActPiezasTotales_REPORTE As String
    Public Property ScrapPiezasTotales_REPORTE As String
    Public Property TotalInners_REPORTE As String
    Public Property TotalOuters_REPORTE As String
End Class
