﻿Imports System.Data
Imports System.Data.OleDb
Module Variables
    ''VAriables publicas
    Structure usuario
        Public user As String
        Public pass As String
        Public level As Integer
        Public reloj As Integer
        Public fechaini As Date
        Public fechafin As Date
    End Structure
    Structure lote
        Public NumLote As String
        Public ModeloLote As String
        Public StatusLote As Integer
        Public Num_Empleado_InicioLote As Integer
        Public Num_Empleado_TerminaLote As Integer
        Public Num_Empleado_SuspendeLote As Integer
        Public fechainicioLote As String
        Public fechafinLote As String
        Public fechasuspendeLote As String
    End Structure
   

    Public usuarios(100) As usuario
    Public lotes As lote
    Public conexion As New OleDbConnection
    Public adaptador As New OleDbDataAdapter
    Public registros As New DataSet
    Public dr As OleDbDataReader
    Public lotenuevo As Boolean

    Public passOK As Boolean
    Public nivel As Integer
    Public archiexcel As String
    Public IDLote As String
    Public Estacion As Integer
    Public Meta As Integer
    Public Modelo As String
    Public ActCajasInternas As Integer
    Public ActCajasExternas As Integer
    Public ExactCajasInternas As Integer
    Public ExactCajasExternas As Integer
    Public Conciliado As Boolean
    Public Usuarioinicial As Integer
    Public UsuarioConciliado As Integer
    Public nombreusuario As String
    Public relojUsuario As Integer
    Public conciliando As Boolean
    Public statuslote As Integer
    Public VALOR_CONCILIAR As String
    Public PIEZAS_TOTALES_LOTES As Integer
    Public PIEZAS_POR_CAJA As Integer ' ANTES ERA INTEGER 12-14-18
    Public CAJAS_INNER_POR_OUTTER As Integer
    Public INNER As Integer = 0
    Public OUTER As Integer = 0
    Public SCRAP_INNER As Boolean = False
    Public SCRAP_OUTER As Boolean = False
    Public IDMODELO As String
    Public ETIQUETA_INNER_L As Boolean = True
    Public ETIQUETA_INNER_M As Boolean = True
    Public ETIQUETA_OUTTER As Boolean
    Public NO_SIRVE_INNER As String = 0
    Public NO_SIRVE_OUTER As String = 0

    Public loteencontrado As Boolean
    Public loteenCSV As Boolean

    Public ToNext As String
    Public IsFinishing As Boolean = False

    Public sum_Inner, Sum_Outer As Integer
    Public usuario_Inicial_Rep As String
    Public LoteNum_Rep As String
    Public Comentario_DeBD, Comentario, Comentario_Ant As String
    Public IsCommented As Boolean
    Public In_Or_Out As String = "In"

    Public PKInn, PKOut As String
    Public Codigo_Barra_Inner_Completo As String
    Public Codigo_Barra_Outter_Completo As String
    Public Path_BD_Red, New_Path_BD_Red As String 'Link donde se encuentra CSV en red

    Public Num_Codigos As Integer
    Public IsProccess As Boolean

    Public campos() As String

End Module
