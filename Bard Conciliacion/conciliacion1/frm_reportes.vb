﻿Imports System.Data
Imports System.Data.OleDb
Imports Microsoft.Reporting.WinForms
Public Class frm_reportes

    Public Detail As New List(Of Class1)()
    'Private Shared Function FillDgv() As List(Of Class1)
    '    '
    '    'Cree una lista generica de la entidad EArticulo
    '    '
    '    Dim listaDATOS As New List(Of Class1)()
    '    '
    '    'Instancie la clase EArticulo para agregar datos a la lista
    '    '
    '    'Establezca valores a cada una de las propiedades
    '    Dim DATOS As New Class1()
    '    DATOS.ActCajasExternasREPORTE = ActCajasExternas
    '    DATOS.ActCajasInternas_REPORTE = ActCajasInternas
    '    DATOS.Comentario_REPORTE = FrmComentarios.TextBox1.Text
    '    DATOS.Estacion_REPORTE = Estacion
    '    DATOS.ExactCajasExternas_REPORTE = ExactCajasExternas
    '    DATOS.ExactCajasInternas_REPORTE = ExactCajasInternas
    '    DATOS.Fecha_Fin_REPORTE = Date.Today
    '    DATOS.Fecha_Ini_REPORTE = Date.Today
    '    DATOS.Lote_REPORTE = LOTE_SCRAP
    '    DATOS.Meta_REPORTE = Meta
    '    DATOS.Nombre_REPORTE = Usuarioinicial
    '    DATOS.Num_Emple_inicio_REPORTE = relojUsuario
    '    DATOS.Num_Emple_Termi_REPORTE = UsuarioConciliado
    '    DATOS.Scrap_Externo_REPORTE = NO_SIRVE_OUTER
    '    DATOS.Scrap_Interno_REPORTE = NO_SIRVE_INNER
    '    DATOS.UsuarioInicial_REPORTE = "MOMBRE"
    '    DATOS.UsuarioConciliado_REPORTE = "NOMBRE"
    '    DATOS.Modelo_reporte = Modelo



    '    listaDATOS.Add(DATOS)

    '    Return listaDATOS
    'End Function

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
        frm_ppal.Show()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If RadioButton1.Checked = True Then
            Dim consulta As String

            If TextBox1.Text <> "" Then
                GENERAR_REPORTE()
                CNN.Open()
                consulta = "SELECT tblComentarios.*, tblConciliar.*, tblLotes.* FROM (tblComentarios INNER JOIN tblConciliar ON tblComentarios.IDConciliar = tblConciliar.IDConciliar) INNER JOIN tblLotes ON tblConciliar.Lote = tblLotes.Lote WHERE tblConciliar.Lote ='" & TextBox1.Text & "'"
                'consulta = "SELECT * From tblConciliar WHERE Lote ='" & TextBox1.Text & "'"    ''para enteros
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(consulta, CNN)
                dr = cmd.ExecuteReader
                If dr.HasRows Then
                    While dr.Read()
                        Label3.Text = dr.Item("Estacion").ToString
                    End While
                    CNN.Close()
                Else
                    CNN.Close()
                    MessageBox.Show("LOTE NO ENCONTRADO")
                End If
                'generar()
                'Else
                '    MessageBox.Show("VALORES MAL")
            End If
        End If

        If RadioButton2.Checked = True Then
            Dim consulta As String

            If DateTimePicker1.Text <> "" And DateTimePicker2.Text <> "" Then
                consulta = "SELECT tblLotes.*, tblConciliar.Conciliado FROM tblConciliar INNER JOIN tblLotes ON tblConciliar.Lote = tblLotes.Lote WHERE ((coinciliado=si))" And "Fecha_Ini=" & Val(DateTimePicker1.Text) And "Fecha_Fin=" & (Val(DateTimePicker2.Text) Or "fecha_Sus=" & Val(DateTimePicker2.Text)) ''para enteros
                Dim cmd1 As OleDbCommand = New OleDbCommand(consulta, conexion)
                Dim read1 As OleDbDataReader = cmd1.ExecuteReader()
                If read1.HasRows Then
                    While read1.Read()
                        Label3.Text = read1.Item("Conciliado").ToString
                    End While
                    CNN.Close()
                End If
                GENERAR_REPORTE()
            Else
                CNN.Close()
                MessageBox.Show("LOTE NO ENCONTRADO")
            End If

        End If


        If RadioButton3.Checked = True Then
            Dim consulta As String

            If DateTimePicker1.Text <> "" And DateTimePicker2.Text <> "" Then
                consulta = "SELECT tblLotes.*, tblConciliar.Conciliado FROM tblConciliar INNER JOIN tblLotes ON tblConciliar.Lote = tblLotes.Lote WHERE ((coinciliado=No))" And "Fecha_Ini=" & Val(DateTimePicker1.Text) And "Fecha_Fin=" & (Val(DateTimePicker2.Text) Or "fecha_Sus=" & Val(DateTimePicker2.Text)) ''para enteros
                Dim cmd1 As OleDbCommand = New OleDbCommand(consulta, conexion)
                Dim read1 As OleDbDataReader = cmd1.ExecuteReader()
                If read1.HasRows Then
                    While read1.Read()
                        Label3.Text = read1.Item("Conciliado").ToString
                    End While
                    CNN.Close()
                End If
                GENERAR_REPORTE()
            Else
                CNN.Close()
                MessageBox.Show("LOTE NO ENCONTRADO")
            End If

        End If

        If RadioButton4.Checked = True Then
            GENERAR_REPORTE()
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        TextBox1.Visible = True
        DateTimePicker1.Visible = False
        DateTimePicker2.Visible = False
        Label1.Visible = True
        Label1.Text = "LOTE"
        Label2.Visible = False
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        TextBox1.Visible = False
        DateTimePicker1.Visible = True
        DateTimePicker2.Visible = True
        Label1.Visible = False
        Label2.Visible = True
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        TextBox1.Visible = False
        DateTimePicker1.Visible = True
        DateTimePicker2.Visible = True
        Label1.Visible = False
        Label2.Visible = True
    End Sub

    Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton4.CheckedChanged
        TextBox1.Visible = True
        DateTimePicker1.Visible = True
        DateTimePicker2.Visible = True
        Label1.Text = " META"
        Label1.Visible = True
        Label2.Visible = True
    End Sub

    Private Sub frm_reportes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        TextBox1.Visible = True


        RadioButton1.Checked = True
        Label1.Visible = True
        DateTimePicker1.Format = DateTimePickerFormat.Short
        DateTimePicker2.Format = DateTimePickerFormat.Short
        DateTimePicker1.Visible = False
        DateTimePicker2.Visible = False
        Label2.Visible = False
    End Sub

End Class