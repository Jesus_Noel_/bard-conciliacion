﻿Imports System.Data
Imports System.Data.OleDb
Imports Excel = Microsoft.Office.Interop.Excel
Imports Interop = Microsoft.Office.Interop.Excel

Public Class frm_prod
    Dim ESCANEADORMODELO As Boolean
    Dim ESCANEADOLOTE As Boolean
    Dim Conciliar_datos As Integer
    Dim lote As String
    Dim leerlote As Boolean
    Dim leermodelo As Boolean
    Dim interior As Integer
    Dim exterior As Integer
    Dim exterior3d As Integer
    Dim faltanetiquetasinnerscrap As Double
    Dim faltancajasinterior As Double
    Dim faltancajasexterior As Integer
    Dim piezas_a_empacar As Integer
    Dim scaneainterior As Boolean
    Dim scaneaexterior As Boolean
    Dim scaneaexterior3D As Boolean
    Dim scaneando As String
    Dim SCRAP As Integer = 0
    Dim VALOR_CONCILIAR As String
    Dim PiezaXcajaActual As Integer
    Dim Is1stIn, Is1stOut As Boolean
    Dim Ver_Codigo_Completo_Inner As Boolean = False
    Dim Ver_Codigo_Completo_Outter As Boolean = False
    Dim Codigos_Restantes As Integer

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonREGRESAR.Click
        Me.Close()
        mantennimiento = False
        Refresh_DB_Timer.Stop()
        frm_ppal.Show()
    End Sub

    Private Sub frm_prod_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If statuslote > 1 Then
            Label15.Text = " PRESIONE EL BOTON ACEPTAR, PARA INICIAR LOTE"
            leerloteconciliando()
            faltancajasinterior = cajasinternasXexterna
            TextBoXTOTALPIEZALOTE.Text = Meta
            TextBoxINERLOTE.Text = ExactCajasInternas
            TextBoxINNERSCAN.Text = ActCajasInternas
            TextBoxINNERSCRAP.Text = NO_SIRVE_INNER
            LabelLOTE.Text = lote_PROD
            LabelMODELO.Text = Modelo
            Label22.Text = Usuarioinicial
            'PIEZAS_POR_CAJA = Math.Truncate(Meta / TextBoxINERLOTE.Text) + 1
            TextBoxOUTERLOTE.Text = ExactCajasExternas
            TextBoxOUTERSCAN.Text = ActCajasExternas
            TextBoxOUTERSCRAP.Text = NO_SIRVE_OUTER
            TextBoxPIEZASSCAN.Text = (Val(TextBoxINNERSCAN.Text) * PIEZAS_POR_CAJA)
            'TextBoxTOTALSCRAP.Text = (Val(TextBoxINNERSCRAP.Text) + Val(TextBoxOUTERSCRAP.Text))
            ' cajasinternasXexterna
            'TOTAL_ETIQUETASINNER_SOBRANTES = (Val(TextBoxINERLOTE.Text) - (Val(TextBoxINNERSCAN.Text) + Val(TextBoxINNERSCRAP.Text)))
            'TOTAL_ETIQUETASOUTER_SOBRANTES = (Val(TextBoxOUTERLOTE.Text) - (Val(TextBoxOUTERSCAN.Text) + Val(TextBoxOUTERSCRAP.Text)))
            'Label15.Text = "LOTE TERMINADO ESCANEE LAS: " & TOTAL_ETIQUETASINNER_SOBRANTES & " Y  " & TOTAL_ETIQUETASOUTER_SOBRANTES & " SOBRANTES PARA TERMINAR EL LOTE"
            'If TOTAL_ETIQUETASINNER_SOBRANTES > 0 Then
            '    TextBoxSCRAP.Focus()
            'ElseIf TOTAL_ETIQUETASOUTER_SOBRANTES > 0 Then
            '    TextBoxSCRAP.Focus()
            'ElseIf TOTAL_ETIQUETASINNER_SOBRANTES > 0 And TOTAL_ETIQUETASOUTER_SOBRANTES > 0 Then
            '    TextBoxSCRAP.Focus()
            'End If
            ButtonCAMBIOUSUARIO.Enabled = False
            ButtoncCODIGONOLEGIBLE.Enabled = False
            ButtonCOMENTARIO.Enabled = False
            ButtonConciliar.Enabled = False
            ButtonSCRAP.Enabled = False
            ButtonSUSPENDER.Visible = False
            ButtonACEPTAR.Focus()
        Else
            faltancajasinterior = cajasinternasXexterna
            Label15.Text = "PRESIONE EL BOTON ACEPTAR, PARA INICIAR LOTE"
            Label22.Text = Usuarioinicial
            LabelMODELO.Text = UCase(IDMODELO)
            LabelLOTE.Text = UCase(IDLote)
            ButtonACEPTAR.Enabled = True
            ButtonACEPTAR.Focus()
            TextBoxINERLOTE.Text = ExactCajasInternas
            TextBoxOUTERLOTE.Text = ExactCajasExternas
            TextBoXTOTALPIEZALOTE.Text = PIEZAS_TOTALES_LOTES
            TextBoxINNERSCAN.Text = ActCajasInternas
            TextBoxOUTERSCAN.Text = ActCajasExternas
            TextBoxPIEZASSCAN.Text = (ActCajasInternas * PIEZAS_POR_CAJA)
            TextBoxOUTERSCRAP.Text = 0
            TextBoxINNERSCRAP.Text = 0
            ButtonConciliar.Enabled = False
            ButtonCAMBIOUSUARIO.Enabled = False
            ButtoncCODIGONOLEGIBLE.Enabled = False
            ButtonCOMENTARIO.Enabled = False
            ButtonSCRAP.Enabled = False
            ButtonSUSPENDER.Visible = False
            ButtonACEPTAR.Focus()
            Refresh_DB_Timer.Stop()
        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Refresh_DB_Timer.Tick
        'If conciliando = False Then
        '    Dim ETIQUETA_INNER As Integer = 0
        '    If TextBoxINERLOTE.Text <> "" And TextBoxOUTERLOTE.Text <> "" And statuslote < 3 Then
        '        TextBoxPIEZASSCAN.Text = (Val(TextBoxINNERSCAN.Text) * PIEZAS_POR_CAJA) '+ Val(TextBox12.Text)
        '        TextBoxTOTALSCRAP.Text = Val(TextBoxOUTERSCRAP.Text) + Val(TextBoxINNERSCRAP.Text)
        '    End If
        'End If
        '  escribirlote()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonConciliar.Click
        nivel = 2
        Refresh_DB_Timer.Stop()
        conciliando = True
        frm_password.ShowDialog()
        Refresh_DB_Timer.Stop()
        TextBox6.Enabled = False
        TextBoxOuters.Enabled = False
        SCRAP_INNER = False
        SCRAP_OUTER = False
        lote = LabelLOTE.Text
        '''''''''''''''''''''' Aunque no se tengan permisos, permite conciliar el reporte'''''''''''''''''''
        If passOK = True Then
            Dim result2 As Integer = MessageBox.Show("¿Sobraron etiquetas para SCRAP?", "", MessageBoxButtons.YesNo)
            If result2 = DialogResult.Yes Then
                IsProccess = False
                ButtonTerminarScrap.Visible = True
                ButtonTerminarScrap.Enabled = True
                IsFinishing = True
                SCRAP_INNER = True
                Label15.Text = "HAGA SCRAP LOS INNER CORRESPONDIENTES"
                TextBoxSCRAP.Visible = True
                TextBoxSCRAP.Focus()
                Me.Refresh()
            Else
                IsProccess = False
                Dim result As Integer = MessageBox.Show("Lote terminado", "TERMINADO", MessageBoxButtons.OK)
                If result = DialogResult.OK Then
                    statuslote = 3
                    escribirlote()
                    'If (Val(TextBoxINERLOTE.Text) = (Val(TextBoxINNERSCAN.Text) + Val(TextBoxINNERSCRAP.Text))) And (Val(TextBoxOUTERLOTE.Text) = (Val(TextBoxOUTERSCAN.Text) + Val(TextBoxOUTERSCRAP.Text))) Then
                    statuslote = 3
                    ButtonConciliar.Enabled = True
                    ButtonConciliar.Focus()
                    If TextBoxINNERSCRAP.Text = "" Then
                        TextBoxINNERSCRAP.Text = 0
                    End If
                    sum_Inner = CInt(TextBoxINNERSCAN.Text) + CInt(TextBoxINNERSCRAP.Text)
                    Sum_Outer = CInt(TextBoxOUTERSCAN.Text) + CInt(TextBoxOUTERSCRAP.Text)
                    LoteNum_Rep = LabelLOTE.Text
                    Modelo = LabelMODELO.Text
                    Buscar_COMENTARIO()
                    ActCajasExternas = TextBoxOUTERSCAN.Text
                    Me.Close()
                    GENERAR_REPORTE()
                End If

               

            End If

        End If
        

    End Sub

    Private Sub Button3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonACEPTAR.Click
        TextBox6.Enabled = True

        Codigos_Restantes = Num_Codigos
        ButtonCAMBIOUSUARIO.Enabled = True
        ButtoncCODIGONOLEGIBLE.Enabled = True
        ButtonCOMENTARIO.Enabled = True
        ButtonSCRAP.Enabled = True
        ButtonSUSPENDER.Visible = True
        ButtonConciliar.Enabled = True
        ButtonACEPTAR.Enabled = False
        ButtonACEPTAR.Visible = False

        If lotenuevo = True Then
            TextBoxINERLOTE.Enabled = False
            TextBoxOUTERLOTE.Enabled = False
            TextBoXTOTALPIEZALOTE.Enabled = False
            TextBoxINNERSCAN.Enabled = False
            TextBoxOUTERSCAN.Enabled = False
            TextBoxPIEZASSCAN.Enabled = False
            statuslote = 1
            escrbirlotenuevo()    ''escribimos info de pantalla en base de datos
            lotenuevo = False
            ButtonConciliar.Enabled = True

        End If
        If TextBoxPIEZASSCAN.Text > 0 Then
            ButtonConciliar.Enabled = True
            If ToNext = "Outter" Then

                Is1stIn = False
                Is1stOut = True

                'If TextBoxINNERSCAN.Text Mod TextBoxOUTERSCAN.Text = 0 Then

                ' End If
                'If (Val(TextBoxPIEZASSCAN.Text) Mod cajasinternasXexterna) = 0 Then
                ' Label15.Text = "ESCANE ETIQUETA OUTER"
                Label15.Text = "ESCANE PK DE ETIQUETA OUTER"
                SCRAP_OUTER = True
                SCRAP_INNER = False
                scaneaexterior = True
                scaneando = " EXTERIOR "
                TextBoxOuters.Visible = True
                TextBoxOuters.Focus()
            Else
                faltancajasinterior = ((TextBoxOUTERSCAN.Text + 1) * cajasinternasXexterna) - TextBoxINNERSCAN.Text

                '   Label15.Text = "ESCANEE LOTE DE ETIQUETA INNER"
                Label15.Text = "ESCANE PK DE ETIQUETA INNER"
                SCRAP_INNER = True
                SCRAP_OUTER = False
                leerlote = True
                TextBox6.Focus()
                scaneainterior = True
                scaneando = " INTERIOR "
                conciliando = False
                Refresh_DB_Timer.Start()
                Is1stIn = True
                Is1stOut = False
            End If
        Else
            Label15.Text = "ESCANEE PK DE ETIQUETA INNER"
            SCRAP_INNER = True
            SCRAP_OUTER = False
            leerlote = True
            TextBox6.Focus()
            scaneainterior = True
            scaneando = " INTERIOR "
            conciliando = False
            Refresh_DB_Timer.Start()
            Is1stIn = True
        End If


    End Sub

    Private Sub TextBox6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox6.KeyPress
        Dim TestPos As Integer
        Dim start_Code As String
        Dim indxOfJ As Integer
        Dim final_code As String



        '  PiezaXcajaActual = 0


        If Val(TextBoxPIEZASSCAN.Text) <= Val(TextBoXTOTALPIEZALOTE.Text) Then
            SCRAP_INNER = True
            SCRAP_OUTER = False

            If (Chr(13) = e.KeyChar) Then



                If Is1stIn = True Then
                    TextBox6.CharacterCasing = CharacterCasing.Upper
                    final_code = TextBox6.Text
                    '  TestPos = InStr(final_code, PKInn)
                    ' Dim camp() As String
                    If campos.Contains(final_code) = True Then
                        TestPos = 1
                    Else
                        TestPos = 0

                    End If


                    If (TestPos > 0) Then
                        Label15.Text = "PK OK"
                        Label15.BackColor = Color.Lime
                        System.Threading.Thread.Sleep(500)
                        Is1stIn = False
                        Is1stOut = True
                        Label15.Update()
                        TextBox6.Update()
                        Codigos_Restantes = Num_Codigos
                        Label15.Text = "ESCANEE LOTE DE ETIQUETA INNER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"
                        Label15.BackColor = Color.Blue
                        System.Threading.Thread.Sleep(250)
                        ToNext = "Inner"
                        In_Or_Out = "In"
                        TextBox6.BackColor = Color.LightGreen
                        TextBox6.Update()
                        System.Threading.Thread.Sleep(250)
                        TextBox6.BackColor = Color.White
                        TextBox6.Text = ""
                        TextBox6.Update()
                        TestPos = 0

                    Else
                        Label15.Text = "ETIQUETA EQUIVOCADA "
                        Label15.BackColor = Color.Red
                        Me.Refresh()
                        TextBox6.BackColor = Color.Orange
                        System.Threading.Thread.Sleep(1000)
                        Label15.Text = "ESCANE PK DE ETIQUETA INNER"
                        Label15.BackColor = Color.Transparent
                        TextBox6.BackColor = Color.White
                        TextBox6.Text = ""
                        TextBox6.Update()

                    End If
                Else
                    If Codigos_Restantes = 1 Then

                        Codigos_Restantes = Num_Codigos
                        TextBox6.CharacterCasing = CharacterCasing.Upper
                        start_Code = TextBox6.Text
                        If Ver_Codigo_Completo_Inner = False Then
                            indxOfJ = InStr(start_Code, "J")
                            If indxOfJ = 0 Then
                                final_code = start_Code
                            ElseIf indxOfJ > 0 Then
                                final_code = Mid(start_Code, indxOfJ, 8)


                            End If
                        Else

                            final_code = start_Code
                        End If




                        If leerlote = True Then
                            If Ver_Codigo_Completo_Inner = False Then
                                TestPos = InStr(final_code, LabelLOTE.Text)
                            Else
                                TestPos = InStr(final_code, Codigo_Barra_Inner_Completo)
                            End If

                            If (TestPos > 0) Then


                                SCRAP_OUTER = False
                                SCRAP_INNER = True
                                Label15.Text = "ETIQUETA OK"

                                Label15.BackColor = Color.Lime
                                System.Threading.Thread.Sleep(500)
                                TextBoxINNERSCAN.Text = Val(TextBoxINNERSCAN.Text) + 1
                                ActCajasInternas = ActCajasInternas + 1
                                faltancajasinterior = faltancajasinterior - 1
                                TextBoxPIEZASSCAN.Text = TextBoxPIEZASSCAN.Text + PIEZAS_POR_CAJA
                                Label15.Update()
                                TextBox6.Update()
                                In_Or_Out = "Out"
                                If Ver_Codigo_Completo_Inner = False Then
                                    Ver_Codigo_Completo_Inner = True
                                    Codigo_Barra_Inner_Completo = TextBox6.Text


                                End If

                                If (faltancajasinterior = 0) Then
                                    If Is1stOut = True Then
                                        SCRAP_INNER = False
                                        SCRAP_OUTER = True

                                        Label15.Text = "ESCANEE PK DE ETIQUETA OUTER"
                                        In_Or_Out = "Out"
                                        Label15.BackColor = Color.Blue
                                        System.Threading.Thread.Sleep(250)
                                        Label15.Update()
                                        TextBoxOuters.Visible = True
                                        TextBoxOuters.Focus()
                                        scaneaexterior = True
                                        leerlote = False
                                        faltancajasinterior = cajasinternasXexterna
                                        ActCajasExternas = ActCajasExternas + 1
                                        ToNext = "Outter"
                                    Else
                                        SCRAP_INNER = False
                                        SCRAP_OUTER = True

                                        Label15.Text = "ESCANEE ETIQUETA OUTER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"
                                        In_Or_Out = "Out"
                                        Label15.BackColor = Color.Blue
                                        System.Threading.Thread.Sleep(250)
                                        Label15.Update()
                                        TextBoxOuters.Visible = True
                                        TextBoxOuters.Focus()
                                        scaneaexterior = True
                                        leerlote = False
                                        faltancajasinterior = cajasinternasXexterna
                                        ActCajasExternas = ActCajasExternas + 1
                                        ToNext = "Outter"
                                    End If

                                Else

                                    Label15.Text = "ESCANEE LOTE DE ETIQUETA INNER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"
                                    Label15.BackColor = Color.Blue
                                    System.Threading.Thread.Sleep(250)
                                    ToNext = "Inner"
                                    In_Or_Out = "In"
                                End If
                            End If

                            If (TestPos = 0) Then

                                Label15.Text = "ETIQUETA EQUIVOCADA "
                                Label15.BackColor = Color.Red
                                TextBox6.BackColor = Color.Orange
                                Me.Refresh()
                                System.Threading.Thread.Sleep(1000)
                                Label15.Text = "ESCANEE LOTE DE ETIQUETA INNER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"
                                Label15.BackColor = Color.Transparent

                                TextBox6.BackColor = Color.White
                                TextBox6.Text = ""
                                TextBox6.Update()
                            End If
                            TextBox6.BackColor = Color.LightGreen
                            TextBox6.Update()
                            System.Threading.Thread.Sleep(250)
                            TextBox6.BackColor = Color.White
                            TextBox6.Text = ""
                            TextBox6.Update()
                            TestPos = 0
                            'Timer1.Start()
                        End If

                    Else
                        Codigos_Restantes = Codigos_Restantes - 1
                        Label15.Text = "ESCANEE LOTE DE ETIQUETA INNER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"

                    End If
                End If




            End If
        End If


    End Sub
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSUSPENDER.Click

        Dim result As Integer = MessageBox.Show("Seguro de SUSPENDER?", "SUSPENDE", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            statuslote = 2 ''suspendido
            Estacion = 1
            escribirlote()
            Button5_Click(0, e)
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCAMBIOUSUARIO.Click
        If MsgBox("Seguro que desea cambiar de usuario", MsgBoxStyle.OkCancel, "Cambio de usuario") = vbOK Then
            escribirlote()
            nivel = 0
            Me.Hide()
            Refresh_DB_Timer.Stop()
            frm_checklist.Show()
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSCRAP.Click
        'TextBoxSCRAP.Visible = True
        'TextBoxSCRAP.Focus()
        'Dim SCRAP As Integer = 0
        IsProccess = True

        If In_Or_Out = "In" Then
            SCRAP_INNER = True
            SCRAP_OUTER = False

        ElseIf In_Or_Out = "Out" Then
            SCRAP_INNER = False
            SCRAP_OUTER = True

        End If
        Refresh_DB_Timer.Stop()
        Dim result As Integer = MessageBox.Show("Seguro hacer scrap?", "SCRAP", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            IsFinishing = False

            If SCRAP_INNER = True Then
                Label15.Text = "ESCANEE SCRAP DE ETIQUETA INNER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"
                TextBoxSCRAP.Visible = True
                TextBoxSCRAP.Focus()
                scrap_produccion = True
                'TextBoxINNERSCRAP.Text = Val(TextBoxINNERSCRAP.Text) + 1)
            ElseIf SCRAP_OUTER = True Then
                Label15.Text = "ESCANEE SCRAP DE ETIQUETA OUTER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"
                TextBoxOuters.Visible = False
                TextBoxSCRAP.Visible = True
                TextBoxSCRAP.Focus()
                scrap_produccion = True
                'TextBoxOUTERSCRAP.Text = Val(TextBoxOUTERSCRAP.Text) + 1
            End If
            '  TextBoxSCRAP.Visible = True
            ' TextBoxSCRAP.Focus()
        End If

        'SCR_INNER = False
        'SCRAP_OUTER = False
        'TextBox6.Focus()
        'Timer1.Start()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCOMENTARIO.Click
        Me.Hide()
        FrmComentarios.Show()
    End Sub

    Private Sub TextBoxOuters_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxOuters.KeyPress
        Dim TestPos1 As Integer
        Dim start_Code As String
        Dim indxOfJ As Integer
        Dim final_code As String
        '  Dim codigos_Restantes
        If scaneaexterior = True Then
            If e.KeyChar = Chr(13) Then
                If Is1stOut = True Then
                    TextBoxOuters.CharacterCasing = CharacterCasing.Upper
                    final_code = TextBoxOuters.Text
                    If campos.Contains(final_code) = True Then
                        TestPos1 = 1
                    Else
                        TestPos1 = 0

                    End If
                    ' TestPos1 = InStr(final_code, PKOut)
                    If (TestPos1 > 0) Then
                        Label15.Text = "PK OK"
                        Label15.BackColor = Color.Lime
                        System.Threading.Thread.Sleep(500)
                        codigos_Restantes = Num_Codigos
                        Is1stOut = False
                        Label15.Update()
                        TextBoxOuters.Update()

                        Label15.Text = "ESCANEE LOTE DE ETIQUETA OUTER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"
                        Label15.BackColor = Color.Blue
                        System.Threading.Thread.Sleep(250)
                        ToNext = "Outter"
                        In_Or_Out = "Out"
                        TextBoxOuters.BackColor = Color.LightGreen
                        TextBoxOuters.Update()
                        System.Threading.Thread.Sleep(250)
                        TextBoxOuters.BackColor = Color.White
                        TextBoxOuters.Text = ""
                        TextBoxOuters.Update()
                        TestPos1 = 0
                    Else
                        Label15.Text = "ETIQUETA EQUIVOCADA "
                        Label15.BackColor = Color.Red
                        TextBox6.BackColor = Color.Orange
                        TextBoxOuters.Text = ""
                        Me.Refresh()
                        System.Threading.Thread.Sleep(1000)
                        Label15.Text = "ESCANEE PK DE ETIQUETA OUTER"
                        Label15.BackColor = Color.Blue
                    End If
                Else
                    If Codigos_Restantes = 1 Then
                        Codigos_Restantes = Num_Codigos
                        TextBoxOuters.CharacterCasing = CharacterCasing.Upper
                        start_Code = TextBoxOuters.Text
                        If Ver_Codigo_Completo_Outter = False Then
                            indxOfJ = InStr(start_Code, "J")
                            If indxOfJ = 0 Then
                                final_code = start_Code
                            Else
                                final_code = Mid(start_Code, indxOfJ, 8)
                            End If
                        Else
                            final_code = start_Code
                        End If

                        If Ver_Codigo_Completo_Outter = False Then
                            TestPos1 = InStr(final_code, LabelLOTE.Text)
                        Else
                            TestPos1 = InStr(final_code, Codigo_Barra_Outter_Completo)
                        End If

                        If (TestPos1 > 0) Then
                            SCRAP_INNER = False
                            SCRAP_OUTER = True
                            Label15.Text = "ETIQUETA OK"
                            TextBoxOuters.BackColor = Color.LightGreen
                            TextBoxOuters.Update()
                            Label15.BackColor = Color.Lime
                            System.Threading.Thread.Sleep(500)
                            Label15.Update()
                            TextBoxOUTERSCAN.Text = Val(TextBoxOUTERSCAN.Text) + 1
                            Label15.BackColor = Color.Lime
                            Label15.Update()
                            TextBoxOuters.Update()
                            scaneainterior = True
                            scaneaexterior = False
                            leerlote = True
                            If Ver_Codigo_Completo_Outter = False Then
                                Ver_Codigo_Completo_Outter = True
                                Codigo_Barra_Outter_Completo = TextBoxOuters.Text
                            End If
                            If TextBoXTOTALPIEZALOTE.Text <> TextBoxPIEZASSCAN.Text Then
                                Label15.Text = "ESCANEE LOTE DE ETIQUETA INNER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"
                                SCRAP_INNER = True
                                SCRAP_OUTER = False
                                ToNext = "Inner"
                                In_Or_Out = "In"
                                Label15.BackColor = Color.Blue
                                System.Threading.Thread.Sleep(250)
                                TextBoxOuters.Text = ""
                                TextBoxOuters.Visible = False
                                Refresh_DB_Timer.Start()
                                TextBox6.Focus()
                            Else
                                TextBoxOuters.Text = ""
                                TextBoxOuters.Visible = False
                                Refresh_DB_Timer.Start()
                                TextBoxSCRAP.Visible = True
                                TextBoxSCRAP.Focus()
                            End If
                        Else
                            Label15.Text = "ETIQUETA EQUIVOCADA "
                            Label15.BackColor = Color.Red
                            TextBoxOuters.BackColor = Color.Orange
                            Me.Refresh()
                            System.Threading.Thread.Sleep(1000)
                            Label15.Text = "ESCANEE LOTE DE ETIQUETA OUTER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"
                            Label15.BackColor = Color.Blue
                            TextBoxOuters.BackColor = Color.White
                            TextBoxOuters.Text = ""
                            TextBoxOuters.Update()
                        End If

                    Else
                        Codigos_Restantes = Codigos_Restantes - 1
                        Label15.Text = "ESCANEE LOTE DE ETIQUETA OUTER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"


                    End If
                   
                End If



            End If
        Else
            TextBox6.Focus()
        End If

    End Sub




    Private Sub TextBoxSCRAP_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxSCRAP.KeyPress
        Dim TestPos1 As Integer
        Dim start_Code As String
        Dim indxOfJ As Integer
        Dim final_code As String
        'SECCION SCRAP INNER Y OUTER (MODO PRODUCCION)
        If e.KeyChar = Chr(13) Then


            If SCRAP_INNER = True Then

                '    Label15.Text = "REALICE SCRAP DE LA ETIQUETA INNER"
                If e.KeyChar = Chr(13) Then

                    If Codigos_Restantes = 1 Then
                        Codigos_Restantes = Num_Codigos
                        TextBoxSCRAP.CharacterCasing = CharacterCasing.Upper
                        start_Code = TextBoxSCRAP.Text
                        If Ver_Codigo_Completo_Inner = False Then
                            indxOfJ = InStr(start_Code, "J")
                            If indxOfJ = 0 Then
                                final_code = start_Code
                            Else
                                final_code = Mid(start_Code, indxOfJ, 8)
                            End If
                        Else
                            final_code = start_Code
                        End If

                        If Ver_Codigo_Completo_Inner = False Then
                            TestPos1 = InStr(final_code, LabelLOTE.Text)
                        Else
                            TestPos1 = InStr(final_code, Codigo_Barra_Inner_Completo)
                        End If

                        If (TestPos1 > 0) Then
                            Label15.Text = "ETIQUETA OK"
                            TextBoxSCRAP.BackColor = Color.LightGreen
                            TextBoxSCRAP.Update()
                            Label15.BackColor = Color.Lime
                            System.Threading.Thread.Sleep(500)
                            Label15.Update()
                            TextBoxINNERSCRAP.Text = Val(TextBoxINNERSCRAP.Text) + 1
                            Label15.BackColor = Color.Lime
                            Label15.Update()
                            TextBoxSCRAP.Update()
                            'SCRAP_INNER_SOBRANTE = True
                            If IsFinishing = False Then
                                SCRAP_INNER = False
                            Else
                                'SCRAP_INNER = TRUE
                            End If

                            leerlote = True
                            TextBoxSCRAP.Text = ""
                            TextBoxSCRAP.Visible = False
                            Refresh_DB_Timer.Start()
                            TextBox6.Focus()
                            Label15.BackColor = Color.Blue
                            Label15.Text = ""
                            If Is1stIn = True Then
                                Label15.Text = "ESCANEE PK DE ETIQUETA INNER"
                            Else

                                If IsProccess = True Then
                                    Label15.Text = "ESCANEE LOTE DE ETIQUETA INNER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"
                                Else
                                    Label15.Text = "ESCANEE SCRAP DE ETIQUETA INNER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"

                                End If
                            End If
                           
                            ' Label15.Text = "REALICE SCRAP DE LA ETIQUETA INNER" & vbCrLf & Codigos_Restantes & " Codigos Restantes"
                            Else
                                Label15.Text = "ETIQUETA EQUIVOCADA "
                                Label15.BackColor = Color.Red
                                TextBoxSCRAP.BackColor = Color.Orange
                                Me.Refresh()
                                System.Threading.Thread.Sleep(1000)
                            Label15.Text = "ESCANEE SCRAP DE ETIQUETA INNER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"
                                Label15.BackColor = Color.Transparent

                                TextBoxSCRAP.BackColor = Color.White
                                TextBoxSCRAP.Text = ""
                                TextBoxSCRAP.Update()
                            End If

                        Else
                            Codigos_Restantes = Codigos_Restantes - 1
                        Label15.Text = "ESCANEE SCRAP DE ETIQUETA INNER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"

                        End If
                  
                End If

                scrap_produccion = True
            End If

            If SCRAP_OUTER = True Then
                'Label15.Text = "REALICE SCRAP DE LA ETIQUETA OUTER"
                If e.KeyChar = Chr(13) Then
                    If Codigos_Restantes = 1 Then
                        Codigos_Restantes = Num_Codigos
                        TextBoxSCRAP.CharacterCasing = CharacterCasing.Upper
                        start_Code = TextBoxSCRAP.Text
                        If Ver_Codigo_Completo_Outter = False Then
                            indxOfJ = InStr(start_Code, "J")
                            If indxOfJ = 0 Then
                                final_code = start_Code
                            Else
                                final_code = Mid(start_Code, indxOfJ, 8)
                            End If
                        Else
                            final_code = start_Code
                        End If

                        If Ver_Codigo_Completo_Outter = False Then
                            TestPos1 = InStr(final_code, LabelLOTE.Text)
                        Else
                            TestPos1 = InStr(final_code, Codigo_Barra_Outter_Completo)
                        End If

                        If (TestPos1 > 0) Then
                            Label15.Text = "ETIQUETA OK"
                            TextBoxSCRAP.BackColor = Color.LightGreen
                            TextBoxSCRAP.Update()
                            Label15.BackColor = Color.Lime
                            System.Threading.Thread.Sleep(500)
                            Label15.Update()
                            TextBoxOUTERSCRAP.Text = Val(TextBoxOUTERSCRAP.Text) + 1
                            Label15.BackColor = Color.Lime
                            Label15.Update()
                            TextBoxSCRAP.BackColor = Color.White
                            TextBoxSCRAP.Text = ""
                            TextBoxSCRAP.Update()
                            TextBoxSCRAP.Update()
                            If IsFinishing = False Then
                                SCRAP_OUTER = False
                            Else
                                'SCRAP_OUTER = TRUE
                                TextBoxSCRAP.BringToFront()

                            End If
                            'SCRAP_OUTER = False
                            TextBoxSCRAP.Visible = False
                            Refresh_DB_Timer.Start()
                            scaneaexterior = True
                            TextBoxOuters.Visible = True
                            TextBoxOuters.Focus()
                            If Is1stOut = True Then
                                Label15.Text = "ESCANEE PK DE ETIQUETA OUTER"
                                Label15.BackColor = Color.Blue
                            Else
                                If IsProccess = True Then
                                    Label15.Text = "ESCANEE LOTE DE LA ETIQUETA OUTER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"
                                    Label15.BackColor = Color.Blue
                                Else
                                    Label15.Text = "ESCANEE SCRAP DE ETIQUETA OUTER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"
                                    Label15.BackColor = Color.Blue
                                End If
                            End If

                           
                        Else
                            Label15.Text = "ETIQUETA EQUIVOCADA "
                            Label15.BackColor = Color.Red
                            TextBoxSCRAP.BackColor = Color.Orange
                            Me.Refresh()
                            System.Threading.Thread.Sleep(1000)
                            Label15.Text = "ESCANEE SCRAP DE ETIQUETA OUTER" & vbCrLf & Codigos_Restantes & " Codigo(s) Restantes"
                            Label15.BackColor = Color.Transparent
                            TextBoxSCRAP.BackColor = Color.White
                            TextBoxSCRAP.Text = ""
                            TextBoxSCRAP.Update()
                        End If

                        Else
                            Codigos_Restantes = Codigos_Restantes - 1
                        Label15.Text = "ESCANEE SCRAP DE ETIQUETA OUTER" & vbCrLf & Codigos_Restantes & " Codigo(s)s Restantes"
                        End If
                 
                End If

                scrap_produccion = False
            End If


            '' SECCION SCRAP INNER USADAS PARA CERRAR LOTE.
            If SCRAP_INNER_SOBRANTE = True Then
                If TOTAL_INNER_SOBRANTES > 0 Then
                    Label15.Text = "REALICE SCRAP DE LA: " & TOTAL_INNER_SOBRANTES & " QUE YA SE USARON"
                    If e.KeyChar = Chr(13) Then
                        TextBoxSCRAP.CharacterCasing = CharacterCasing.Upper
                        start_Code = TextBoxINNERSCRAP.Text
                        indxOfJ = InStr(start_Code, "J")
                        If indxOfJ = 0 Then
                            final_code = start_Code
                        Else
                            final_code = Mid(start_Code, indxOfJ, 8)
                        End If
                        'final_code = Mid(start_Code, indxOfJ, 8)
                        TestPos1 = InStr(final_code, LabelLOTE.Text)
                        If (TestPos1 > 0) Then
                            Label15.Text = "ETIQUETA OK"
                            TextBoxSCRAP.BackColor = Color.LightGreen
                            TextBoxSCRAP.Update()
                            Label15.BackColor = Color.Lime
                            System.Threading.Thread.Sleep(500)
                            Label15.Update()
                            TextBoxINNERSCAN.Text = Val(TextBoxINNERSCAN.Text) - 1
                            TOTAL_INNER_SOBRANTES = TOTAL_INNER_SOBRANTES - 1
                            Label15.BackColor = Color.Lime
                            Label15.Update()
                            TextBoxSCRAP.Update()
                            SCRAP_INNER_SOBRANTE = True
                            If TOTAL_INNER_SOBRANTES > 0 Then
                                Label15.Text = "REALICE SCRAP DE LA: " & TOTAL_INNER_SOBRANTES & " QUE YA SE USARON"

                            Else
                                Label15.Text = "TERMINO SCRAP DE ETIQUETAS INNER"
                            End If

                            Label15.BackColor = Color.Blue
                            System.Threading.Thread.Sleep(250)
                            TextBoxSCRAP.Text = ""
                            TextBoxSCRAP.Visible = False
                            Refresh_DB_Timer.Start()
                            TextBoxSCRAP.Focus()
                        Else
                            Label15.Text = "ETIQUETA EQUIVOCADA "
                            Label15.BackColor = Color.Red
                            TextBoxSCRAP.BackColor = Color.Orange
                            System.Threading.Thread.Sleep(500)
                            TextBoxSCRAP.BackColor = Color.White
                            TextBoxSCRAP.Text = ""
                            TextBoxSCRAP.Update()
                        End If
                    End If
                Else
                    SCRAP_INNER_SOBRANTE = False
                End If
            End If



            'SSECCION SCRAP INNER Y OUTER NO UTILIZADAS (SOBRANTES)
            If ETIQUETAS_SOBRANTES_INNER = True Then
                If TOTAL_ETIQUETASINNER_SOBRANTES > 0 Then
                    Label15.Text = "REALICE SCRAP DE LAS: " & TOTAL_ETIQUETASINNER_SOBRANTES & " INNER SOBRANTES"
                    If e.KeyChar = Chr(13) Then
                        TextBoxSCRAP.CharacterCasing = CharacterCasing.Upper
                        start_Code = TextBoxINNERSCRAP.Text
                        indxOfJ = InStr(start_Code, "J")
                        If indxOfJ = 0 Then
                            final_code = start_Code
                        Else
                            final_code = Mid(start_Code, indxOfJ, 8)
                        End If
                        'final_code = Mid(start_Code, indxOfJ, 8)
                        TestPos1 = InStr(final_code, LabelLOTE.Text)
                        If (TestPos1 > 0) Then
                            Label15.Text = "ETIQUETA OK"
                            TextBoxSCRAP.BackColor = Color.LightGreen
                            TextBoxSCRAP.Update()
                            Label15.BackColor = Color.Lime
                            System.Threading.Thread.Sleep(500)
                            Label15.Update()
                            TextBoxINNERSCRAP.Text = Val(TextBoxINNERSCRAP.Text) + 1
                            TOTAL_ETIQUETASINNER_SOBRANTES = TOTAL_ETIQUETASINNER_SOBRANTES - 1
                            Label15.BackColor = Color.Lime
                            Label15.Update()
                            TextBoxSCRAP.Update()
                            ETIQUETAS_SOBRANTES_INNER = True
                            If TOTAL_ETIQUETASINNER_SOBRANTES > 0 Then
                                Label15.Text = "REALICE SCRAP DE LAS: " & TOTAL_ETIQUETASINNER_SOBRANTES & " INNER SOBRANTES"

                            Else
                                Label15.Text = "TERMINO SCRAP DE ETIQUETAS INNER"

                                TOTAL_ETIQUETASOUTER_SOBRANTES = (Val(TextBoxOUTERLOTE.Text) - (Val(TextBoxOUTERSCAN.Text) + Val(TextBoxOUTERSCRAP.Text)))

                                If TOTAL_ETIQUETASOUTER_SOBRANTES > 0 Then
                                    Label15.Text = Label15.Text & vbCrLf & "REALICE SCRAP DE LAS: " & TOTAL_ETIQUETASOUTER_SOBRANTES & "OUTERS SOBRANTES"
                                End If
                            End If


                            Label15.BackColor = Color.Blue
                            System.Threading.Thread.Sleep(250)
                            TextBoxSCRAP.Text = ""
                            'TextBoxSCRAP.Visible = False
                            Refresh_DB_Timer.Start()
                            TextBoxSCRAP.Focus()
                        Else
                            Label15.Text = "ETIQUETA EQUIVOCADA "
                            Label15.BackColor = Color.Red
                            TextBoxSCRAP.BackColor = Color.Orange
                            System.Threading.Thread.Sleep(500)
                            TextBoxSCRAP.BackColor = Color.White
                            TextBoxSCRAP.Text = ""
                            TextBoxSCRAP.Update()
                            TextBoxSCRAP.Focus()
                        End If
                    End If

                ElseIf TOTAL_ETIQUETASINNER_SOBRANTES = 0 Then
                    Label15.Text = "SCRAP DE ETIQUETAS INNER TERMINADO"
                    TOTAL_INNER_SOBRANTES = 0
                    ETIQUETAS_SOBRANTES_INNER = False
                    ETIQUETAS_SOBRANTES_OUTER = True
                End If
            ElseIf ETIQUETAS_SOBRANTES_OUTER = True Then
                If TOTAL_ETIQUETASOUTER_SOBRANTES > 0 Then
                    Label15.Text = "REALICE SCRAP DE LAS: " & TOTAL_ETIQUETASOUTER_SOBRANTES & " OUTER SOBRANTES"
                    If e.KeyChar = Chr(13) Then
                        TextBoxSCRAP.CharacterCasing = CharacterCasing.Upper
                        start_Code = TextBoxINNERSCRAP.Text
                        indxOfJ = InStr(start_Code, "J")
                        If indxOfJ = 0 Then
                            final_code = start_Code
                        Else
                            final_code = Mid(start_Code, indxOfJ, 8)
                        End If
                        'final_code = Mid(start_Code, indxOfJ, 8)
                        TestPos1 = InStr(final_code, LabelLOTE.Text)
                        If (TestPos1 > 0) Then
                            Label15.Text = "ETIQUETA OK"
                            TextBoxSCRAP.BackColor = Color.LightGreen
                            TextBoxSCRAP.Update()
                            Label15.BackColor = Color.Lime
                            System.Threading.Thread.Sleep(500)
                            Label15.Update()
                            TextBoxOUTERSCRAP.Text = Val(TextBoxOUTERSCRAP.Text) + 1
                            TOTAL_ETIQUETASOUTER_SOBRANTES = TOTAL_ETIQUETASOUTER_SOBRANTES - 1
                            TextBoxTOTALSCRAP.Text = (Val(TextBoxINNERSCRAP.Text) + Val(TextBoxOUTERSCRAP.Text))
                            Label15.BackColor = Color.Lime
                            Label15.Update()
                            TextBoxSCRAP.Update()
                            ETIQUETAS_SOBRANTES_OUTER = True
                            ' Label15.BackColor = Color.Blue
                            If TOTAL_ETIQUETASOUTER_SOBRANTES > 0 Then
                                Label15.Text = "REALICE SCRAP DE LAS: " & TOTAL_ETIQUETASOUTER_SOBRANTES & " OUTER SOBRANTES"

                            Else
                                Label15.Text = "TERMINO SCRAP DE ETIQUETAS OUTER"
                            End If
                            Label15.BackColor = Color.Blue
                            System.Threading.Thread.Sleep(250)
                            TextBoxSCRAP.Text = ""
                            'TextBoxSCRAP.Visible = False
                            Refresh_DB_Timer.Start()
                            TextBoxSCRAP.Focus()
                        Else
                            Label15.Text = "ETIQUETA EQUIVOCADA "
                            Label15.BackColor = Color.Red
                            TextBoxSCRAP.BackColor = Color.Orange
                            System.Threading.Thread.Sleep(500)
                            TextBoxSCRAP.BackColor = Color.White
                            TextBoxSCRAP.Text = ""
                            TextBoxSCRAP.Update()
                            TextBoxSCRAP.Focus()

                        End If
                    End If
                Else
                    Label15.Text = "SCRAP ETIQUETAS OUTER TERMINADO"
                    ETIQUETAS_SOBRANTES_OUTER = False
                End If
                TextBoxSCRAP.Visible = True
                TextBoxSCRAP.Focus()

            End If
            If scrap_produccion = True And SCRAP_INNER = False And SCRAP_OUTER = False Then
                TextBox6.Focus()
                TextBoxSCRAP.Visible = False
                TextBoxOuters.Visible = False
                'leerlote = True
            ElseIf scaneaexterior = True And SCRAP_INNER = False And SCRAP_OUTER = False Then
                TextBoxOuters.Visible = True
                TextBoxOuters.Focus()
            Else
                TextBoxSCRAP.Enabled = True

                TextBoxSCRAP.Visible = True
                TextBoxSCRAP.Focus()

            End If

            'If ETIQUETAS_SOBRANTES_OUTER = False And ETIQUETAS_SOBRANTES_INNER = False And SCRAP_INNER_SOBRANTE = False And scrap_produccion = False And scaneaexterior = False Then
            '    'ButtonConciliar.Focus()
            '    ActCajasExternas = TextBoxOUTERLOTE.Text
            '    ActCajasInternas = TextBoxINERLOTE.Text
            '    Estacion = 1
            '    ExactCajasExternas = TextBoxOUTERSCAN.Text
            '    ExactCajasInternas = TextBoxINNERSCAN.Text
            '    LOTE_SCRAP = LabelLOTE.Text
            '    Meta = TextBoXTOTALPIEZALOTE.Text
            '    Usuarioinicial = Label22.Text
            '    relojUsuario = Label22.Text
            '    UsuarioConciliado = Label22.Text
            '    NO_SIRVE_OUTER = TextBoxOUTERSCRAP.Text
            '    NO_SIRVE_INNER = TextBoxINNERSCRAP.Text
            '    Modelo = LabelMODELO.Text
            '    GENERAR_REPORTE()
            '    Me.Close()
            '    'frm_reportes.Show()
            'End If

        End If
    End Sub

    Private Sub TextBoxOUTERSCAN_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxOUTERSCAN.TextChanged
        ' If Val(TextBoxPIEZASSCAN.Text) = Val(TextBoXTOTALPIEZALOTE.Text) And TextBoXTOTALPIEZALOTE.Text <> "" And TextBoxPIEZASSCAN.Text <> "" And (Val(TextBoxOUTERSCAN.Text) * 10) = Val(TextBoXTOTALPIEZALOTE.Text) Then ' And FALTANOUTER = True 
        If Val(TextBoxPIEZASSCAN.Text) = Val(TextBoXTOTALPIEZALOTE.Text) And TextBoXTOTALPIEZALOTE.Text <> "" And TextBoxPIEZASSCAN.Text <> "" And ((TextBoxPIEZASSCAN.Text) = TextBoXTOTALPIEZALOTE.Text) Then ' And FALTANOUTER = True 

            Refresh_DB_Timer.Stop()
            TextBox6.Enabled = False
            TextBoxOuters.Enabled = False
            SCRAP_INNER = False
            SCRAP_OUTER = False
            lote = LabelLOTE.Text
            Dim result2 As Integer = MessageBox.Show("¿Sobraron etiquetas para SCRAP?", "", MessageBoxButtons.YesNo)
            If result2 = DialogResult.Yes Then
                ButtonTerminarScrap.Visible = True
                ButtonTerminarScrap.Enabled = True
                IsProccess = False
                ButtonTerminarScrap.Enabled = True
                IsFinishing = True
                SCRAP_INNER = True
                Label15.Text = "HAGA SCRAP LOS INNER CORRESPONDIENTES"
                TextBoxSCRAP.Visible = True
                TextBoxSCRAP.Focus()
                Me.Refresh()
            Else
                IsProccess = False
                Dim result As Integer = MessageBox.Show("Lote terminado", "TERMINADO", MessageBoxButtons.OK)
                If result = DialogResult.OK Then
                    statuslote = 3
                    escribirlote()
                    'If (Val(TextBoxINERLOTE.Text) = (Val(TextBoxINNERSCAN.Text) + Val(TextBoxINNERSCRAP.Text))) And (Val(TextBoxOUTERLOTE.Text) = (Val(TextBoxOUTERSCAN.Text) + Val(TextBoxOUTERSCRAP.Text))) Then
                    statuslote = 3
                    ButtonConciliar.Enabled = True
                    ButtonConciliar.Focus()
                    If TextBoxINNERSCRAP.Text = "" Then
                        TextBoxINNERSCRAP.Text = 0
                    End If
                    NO_SIRVE_INNER = TextBoxINNERSCRAP.Text
                    NO_SIRVE_OUTER = TextBoxOUTERSCRAP.Text
                    sum_Inner = CInt(TextBoxINNERSCAN.Text) + CInt(TextBoxINNERSCRAP.Text)
                    Sum_Outer = CInt(TextBoxOUTERSCAN.Text) + CInt(TextBoxOUTERSCRAP.Text)
                    LoteNum_Rep = LabelLOTE.Text
                    Modelo = LabelMODELO.Text
                    Buscar_COMENTARIO()
                    ActCajasExternas = TextBoxOUTERSCAN.Text
                    Me.Close()
                    GENERAR_REPORTE()
                    'frm_reportes.Show()
                    'ElseIf (Val(TextBoxINERLOTE.Text) <> (Val(TextBoxINNERSCAN.Text) + Val(TextBoxINNERSCRAP.Text))) And (Val(TextBoxOUTERLOTE.Text) = (Val(TextBoxOUTERSCAN.Text) + Val(TextBoxOUTERSCRAP.Text))) Then
                    '    TOTAL_ETIQUETASINNER_SOBRANTES = (Val(TextBoxINERLOTE.Text) - (Val(TextBoxINNERSCAN.Text) + Val(TextBoxINNERSCRAP.Text)))
                    '    ETIQUETAS_SOBRANTES_INNER = True
                    '    TextBoxSCRAP.Visible = True
                    '    'SCRAP_INNER = True
                    '    Label15.Text = "HAGA SCRAP: " & TOTAL_ETIQUETASINNER_SOBRANTES & "INNER"
                    '    TextBoxSCRAP.Visible = True
                    '    TextBoxSCRAP.Focus()
                    'ElseIf (Val(TextBoxOUTERLOTE.Text) <> (Val(TextBoxOUTERSCAN.Text) + Val(TextBoxOUTERSCRAP.Text))) And (Val(TextBoxINERLOTE.Text) = (Val(TextBoxINNERSCAN.Text) + Val(TextBoxINNERSCRAP.Text))) Then
                    '    TOTAL_ETIQUETASOUTER_SOBRANTES = (Val(TextBoxOUTERLOTE.Text) - (Val(TextBoxOUTERSCAN.Text) + Val(TextBoxOUTERSCRAP.Text)))
                    '    ETIQUETAS_SOBRANTES_OUTER = True
                    '    TextBoxSCRAP.Visible = True
                    '    'SCRAP_OUTER = True
                    '    Label15.Text = "HAGA SCRAP: " & TOTAL_ETIQUETASOUTER_SOBRANTES & "OUTER"
                    '    TextBoxSCRAP.Focus()
                    'ElseIf (Val(TextBoxINERLOTE.Text) <> (Val(TextBoxINNERSCAN.Text) + Val(TextBoxINNERSCRAP.Text))) And (Val(TextBoxOUTERLOTE.Text) <> (Val(TextBoxOUTERSCAN.Text) + Val(TextBoxOUTERSCRAP.Text))) Then
                    '    TOTAL_ETIQUETASINNER_SOBRANTES = (Val(TextBoxINERLOTE.Text) - (Val(TextBoxINNERSCAN.Text) + Val(TextBoxINNERSCRAP.Text)))
                    '    TOTAL_ETIQUETASOUTER_SOBRANTES = (Val(TextBoxOUTERLOTE.Text) - (Val(TextBoxOUTERSCAN.Text) + Val(TextBoxOUTERSCRAP.Text)))
                    '    Label15.Text = "HAGA SCRAP: " & TOTAL_ETIQUETASINNER_SOBRANTES & "INNER"
                    '    ETIQUETAS_SOBRANTES_INNER = True
                    '    ETIQUETAS_SOBRANTES_OUTER = True
                    '    TextBoxSCRAP.Visible = True
                    '    TextBoxSCRAP.Focus()
                    'End If
                End If
            End If





            'End If


            'Dim CERRARLOTER As Integer
            'CERRARLOTER = (Val(TextBoxOUTERSCAN.Text) * (PIEZAS_POR_CAJA * CAJAS_INNER_POR_OUTTER))
            'If TextBoxOUTERSCAN.Text > 0 Then
            '    If CERRARLOTER = Val(TextBoxPIEZASSCAN.Text) Then
            '        FALTANOUTER = True
            '        TextBox15_TextChanged(Nothing, Nothing)
            '    End If
        End If
    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtoncCODIGONOLEGIBLE.Click

        NO_LEGIBLE = NO_LEGIBLE + 1

        If NO_LEGIBLE >= 3 Then
            Refresh_DB_Timer.Stop()
            Dim result As Integer = MessageBox.Show("YA HA INGRESADO: " & NO_LEGIBLE & " ETIQUETAS MANUALMENTE, LLAME AL SUPERVISOR", "LLAME AL JEFE DE GRUPO", MessageBoxButtons.OKCancel)
            'If result = DialogResult.Yes Then MsgBox("PARA INGRESAR OTRA ETIQUETA NO LEGIBLE, LLAME AL JEFE DE GRUPO", vbOKCancel, "LLAME AL JEFE DE GRUPO")
            If result = vbOK Then
                nivel = 2
                mantennimiento = False
                frm_password.ShowDialog()
                If passOK = True Then
                    faltancajasinterior = (Val(TextBoxPIEZASSCAN.Text) Mod cajasinternasXexterna)
                    If (faltancajasinterior = 0.0) And (TextBoxPIEZASSCAN.Text <> "0") Then
                        ETIQUETA_NO_LEGIBLE_OUTER = ETIQUETA_NO_LEGIBLE_OUTER + 1
                        TextBoxOUTERSCRAP.Text = TextBoxOUTERSCRAP.Text + 1
                        TextBoxOuters.Focus()
                        Refresh_DB_Timer.Start()
                    Else
                        ETIQUETA_NO_LEGIBLE_INNER = ETIQUETA_NO_LEGIBLE_INNER + 1
                        TextBoxINNERSCRAP.Text = TextBoxINNERSCRAP.Text + 1
                        TextBox6.Focus()
                        Refresh_DB_Timer.Start()
                    End If
                End If
            Else
                TextBox6.Focus()
                Refresh_DB_Timer.Start()
            End If

        Else
            Refresh_DB_Timer.Stop()
            faltancajasinterior = (Val(TextBoxPIEZASSCAN.Text) Mod cajasinternasXexterna)
            If (faltancajasinterior = 5.0) And (TextBoxPIEZASSCAN.Text <> "0") Then
                ETIQUETA_NO_LEGIBLE_OUTER = ETIQUETA_NO_LEGIBLE_OUTER + 1
                TextBoxOUTERSCRAP.Text = TextBoxOUTERSCRAP.Text + 1
                TextBoxOuters.Focus()
                Refresh_DB_Timer.Start()
            Else
                ETIQUETA_NO_LEGIBLE_INNER = ETIQUETA_NO_LEGIBLE_INNER + 1
                TextBoxINNERSCRAP.Text = TextBoxINNERSCRAP.Text + 1
                TextBox6.Focus()
                Refresh_DB_Timer.Start()
            End If
        End If
    End Sub



  


    Private Sub TerminarScrap_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTerminarScrap.Click
        If SCRAP_INNER = True And SCRAP_OUTER = False Then
            SCRAP_INNER = False
            SCRAP_OUTER = True
            Label15.Text = "HAGA SCRAP LOS OUTER CORRESPONDIENTES"


        ElseIf SCRAP_INNER = False And SCRAP_OUTER = True Then
            IsFinishing = False
            SCRAP_OUTER = False
            Dim result As Integer = MessageBox.Show("Lote terminado", "TERMINADO", MessageBoxButtons.OK)
            If result = DialogResult.OK Then
                statuslote = 3
                escribirlote()
                'If (Val(TextBoxINERLOTE.Text) = (Val(TextBoxINNERSCAN.Text) + Val(TextBoxINNERSCRAP.Text))) And (Val(TextBoxOUTERLOTE.Text) = (Val(TextBoxOUTERSCAN.Text) + Val(TextBoxOUTERSCRAP.Text))) Then
                statuslote = 3
                ButtonConciliar.Enabled = True
                ButtonConciliar.Focus()
                If TextBoxINNERSCRAP.Text = "" Then
                    TextBoxINNERSCRAP.Text = 0
                End If
                NO_SIRVE_INNER = TextBoxINNERSCRAP.Text
                NO_SIRVE_OUTER = TextBoxOUTERSCRAP.Text
                sum_Inner = CInt(TextBoxINNERSCAN.Text) + CInt(TextBoxINNERSCRAP.Text)
                Sum_Outer = CInt(TextBoxOUTERSCAN.Text) + CInt(TextBoxOUTERSCRAP.Text)
                LoteNum_Rep = LabelLOTE.Text
                Modelo = LabelMODELO.Text
                Buscar_COMENTARIO()
                ActCajasExternas = TextBoxOUTERSCAN.Text

                Me.Close()
                GENERAR_REPORTE()
            End If

        End If

        TextBoxSCRAP.Focus()
       



    End Sub

   

End Class

