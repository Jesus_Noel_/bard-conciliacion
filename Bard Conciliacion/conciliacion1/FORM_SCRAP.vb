﻿Public Class FORM_SCRAP

    Private Sub FORM_SCRAP_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If FALTAN_INNER <> 0 And FALTAN_OUTER <> 0 Then
            SCRAP_OUTER = True
            SCRAP_INNER = True
            TextBoxOuters.Visible = True
            TextBoxOUTTERLOTE.Visible = True
            TextBoxINNERLOTE.Visible = True
            TextBoxSCRAPINNER.Visible = True
            TextBoxINNERLOTE.Text = ExactCajasInternas
            TextBoxOUTTERLOTE.Text = ExactCajasExternas
            TextBoxSCRAPINNER.Text = NO_SIRVE_INNER
            TextBoxSCRAPPOUTER.Text = NO_SIRVE_OUTER
            TextBoxTOTALSCRAP.Text = (Val(TextBoxSCRAPINNER.Text) + Val(TextBoxSCRAPPOUTER.Text))
            Label15.Text = "ESCANEE ETIQUETA INNER"
        ElseIf FALTAN_INNER <> 0 And FALTAN_OUTER = 0 Then
            SCRAP_OUTER = False
            SCRAP_INNER = True
            TextBoxOuters.Visible = False
            TextBoxOUTTERLOTE.Visible = False
            TextBoxINNERLOTE.Text = ExactCajasInternas
            'TextBoxOUTTERLOTE.Text = ExactCajasExternas
            TextBoxSCRAPINNER.Text = NO_SIRVE_INNER
            'TextBoxSCRAPPOUTER.Text = NO_SIRVE_OUTER
            TextBoxTOTALSCRAP.Text = (Val(TextBoxSCRAPINNER.Text))
            Label15.Text = "ESCANEE ETIQUETA INNER"
        ElseIf FALTAN_OUTER <> 0 And FALTAN_INNER = 0 Then
            SCRAP_OUTER = True
            SCRAP_INNER = False
            TextBoxINNERLOTE.Visible = False
            TextBoxSCRAPINNER.Visible = False
            TextBoxINNERLOTE.Text = ExactCajasInternas
            TextBoxOUTTERLOTE.Text = ExactCajasExternas
            'TextBoxSCRAPINNER.Text = NO_SIRVE_INNER
            TextBoxSCRAPPOUTER.Text = NO_SIRVE_OUTER
            'TextBoxTOTALSCRAP.Text = (Val(TextBoxSCRAPINNER.Text) + Val(TextBoxSCRAPPOUTER.Text))
            Label15.Text = "ESCANEE ETIQUETA OUTER"
        End If
        Timer2.Start()
        TextBoxSCRAP.Focus()
    End Sub

    Private Sub TextBoxSCRAP_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxSCRAP.KeyPress
        Dim TESPOSINNER, TESPOSOUTTER As Integer
        Dim FALTANETIINNER As Integer = 0
        Dim FALTANETIOUTER As Integer = 0
        If SCRAP_INNER = True And SCRAP_OUTER = True Then
            If e.KeyChar = Chr(13) Then
                'FALTANETIINNER = (Val(TextBoxINNERLOTE.Text) - Val(TextBoxSCRAPINNER.Text))
                'FALTANETIOUTER = (Val(TextBoxOUTTERLOTE.Text) - Val(TextBoxSCRAPPOUTER.Text))
                TextBoxSCRAP.CharacterCasing = CharacterCasing.Upper
                TESPOSINNER = InStr(TextBoxSCRAP.Text, LOTE_SCRAP)
                If (TESPOSINNER > 0) Then

                    Label15.Text = "ETIQUETA OK FALTAN: " & FALTAN_INNER & " ETIQUETAS POR ESCANNEAR"
                    TextBoxSCRAP.BackColor = Color.LightGreen
                    TextBoxSCRAP.Update()
                    Label15.BackColor = Color.Lime
                    System.Threading.Thread.Sleep(1000)
                    'Label15.Update()
                    TextBoxSCRAPINNER.Text = Val(TextBoxSCRAPINNER.Text) + 1
                    Label15.BackColor = Color.Lime
                    Label15.Update()
                    TextBoxSCRAP.Update()
                    FALTANETIINNER = FALTAN_INNER - TextBoxSCRAPINNER.Text
                    'If FALTAN_INNER = 0 Then
                    '    SCRAP_INNER = False
                    '    SCRAP_OUTER = True
                    'End If
                    Label15.Text = "ESCANEE ETIQUETA INNER"
                    Label15.BackColor = Color.Blue
                    System.Threading.Thread.Sleep(250)
                    TextBoxSCRAP.Text = ""
                    'TextBoxSCRAP.Visible = False
                End If
                If FALTANETIINNER < 0 Then
                    Label15.Text = "ESCANEE ETIQUETA OUTER"
                    Label15.BackColor = Color.Blue
                    System.Threading.Thread.Sleep(250)
                    TextBoxSCRAP.Text = ""
                    If e.KeyChar = Chr(13) Then
                        TextBoxSCRAP.CharacterCasing = CharacterCasing.Upper
                        TESPOSOUTTER = InStr(TextBoxSCRAP.Text, LOTE_SCRAP)
                        If (TESPOSINNER > 0) Then
                            Label15.Text = "ETIQUETA OK FALTAN: " & FALTAN_OUTER & " POR ESCANNEAR"
                            TextBoxSCRAP.BackColor = Color.LightGreen
                            TextBoxSCRAP.Update()
                            Label15.BackColor = Color.Lime
                            System.Threading.Thread.Sleep(500)
                            'Label15.Update()
                            TextBoxSCRAPPOUTER.Text = Val(TextBoxSCRAPPOUTER.Text) + 1
                            Label15.BackColor = Color.Lime
                            Label15.Update()
                            TextBoxSCRAP.Update()
                            Label15.Text = "ESCANEE ETIQUETA OUTER"
                            Label15.BackColor = Color.Blue
                            System.Threading.Thread.Sleep(250)
                            TextBoxSCRAP.Text = ""
                            '       TextBoxSCRAP.Visible = False
                        End If
                    End If
                End If
                If SCRAP_INNER = True And SCRAP_OUTER = False Then
                    If e.KeyChar = Chr(13) Then
                        'FALTANETIINNER = (Val(TextBoxINNERLOTE.Text) - Val(TextBoxSCRAPINNER.Text))

                        TextBoxSCRAP.CharacterCasing = CharacterCasing.Upper
                        TESPOSINNER = InStr(TextBoxSCRAP.Text, LOTE_SCRAP)
                        If (TESPOSINNER > 0) Then
                            Label15.Text = "ETIQUETA OK FALTAN: " & FALTAN_INNER & " POR ESCANNEAR"
                            TextBoxSCRAP.BackColor = Color.LightGreen
                            TextBoxSCRAP.Update()
                            Label15.BackColor = Color.Lime
                            System.Threading.Thread.Sleep(1000)
                            Label15.Update()
                            TextBoxSCRAPINNER.Text = Val(TextBoxSCRAPINNER.Text) + 1
                            Label15.BackColor = Color.Lime
                            Label15.Update()
                            TextBoxSCRAP.Update()
                            FALTAN_INNER = TextBoxSCRAPINNER.Text - FALTAN_INNER
                            If FALTAN_INNER = 0 Then
                                SCRAP_INNER = False
                                SCRAP_OUTER = True
                            End If
                            Label15.Text = "ESCANEE ETIQUETA INNER"
                            Label15.BackColor = Color.Blue
                            System.Threading.Thread.Sleep(250)
                            TextBoxSCRAP.Text = ""
                            '      TextBoxSCRAP.Visible = False
                        Else
                            Label15.Text = "ETIQUETA EQUIVOCADA "
                            Label15.BackColor = Color.Red
                            TextBoxSCRAP.BackColor = Color.Orange
                            System.Threading.Thread.Sleep(500)
                            TextBoxSCRAP.BackColor = Color.White
                            TextBoxSCRAP.Text = ""
                            TextBoxSCRAP.Update()
                        End If
                    End If
               
                End If
            End If
        End If
        If SCRAP_OUTER = True And SCRAP_INNER = False Then
            If e.KeyChar = Chr(13) Then
                'FALTANETIOUTER = (Val(TextBoxOUTTERLOTE.Text) - Val(TextBoxSCRAPPOUTER.Text))
                TextBoxSCRAP.CharacterCasing = CharacterCasing.Upper
                TESPOSOUTTER = InStr(TextBoxSCRAP.Text, LOTE_SCRAP)
                If (TESPOSOUTTER > 0) Then
                    System.Threading.Thread.Sleep(500)
                    Label15.Text = "ETIQUETA OK FALTAN: " & FALTAN_OUTER & " POR ESCANNEAR"
                    TextBoxSCRAP.BackColor = Color.LightGreen
                    TextBoxSCRAP.Update()
                    Label15.BackColor = Color.Lime
                    System.Threading.Thread.Sleep(500)
                    Label15.Update()
                    TextBoxSCRAPPOUTER.Text = Val(TextBoxSCRAPPOUTER.Text) + 1
                    Label15.BackColor = Color.Lime
                    Label15.Update()
                    TextBoxSCRAP.Update()
                    Label15.Text = "ESCANEE ETIQUETA OUTER"
                    Label15.BackColor = Color.Blue
                    System.Threading.Thread.Sleep(250)
                    TextBoxSCRAP.Text = ""
                    'TextBoxSCRAP.Visible = False
                Else
                    Label15.Text = "ETIQUETA EQUIVOCADA "
                    Label15.BackColor = Color.Red
                    TextBoxSCRAP.BackColor = Color.Orange
                    System.Threading.Thread.Sleep(500)
                    TextBoxSCRAP.BackColor = Color.White
                    TextBoxSCRAP.Text = ""
                    TextBoxSCRAP.Update()
                    Timer2.Start()
                End If
            End If
        End If
        Timer2.Start()
    End Sub

    Private Sub ButtonAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAceptar.Click
        Me.Close()
        frm_reportes.Show()
    End Sub

    Private Sub ButtonRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRegresar.Click
        Me.Close()
        Frm_datos_Lote.Show()
    End Sub

    'Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    '    If (Val(TextBoxSCRAPINNER.Text) = Val(TextBoxINNERLOTE.Text)) And (Val(TextBoxSCRAPPOUTER.SelectedText) = Val(TextBoxOUTTERLOTE.Text)) Then
    '        NO_SIRVE_INNER = TextBoxSCRAPINNER.Text
    '        NO_SIRVE_OUTER = TextBoxSCRAPPOUTER.Text
    '        Me.Close()
    '        frm_prod.Show()
    '        escribirlote()
    '    Else
    '        escribirlote()
    '    End If
    '    TextBoxSCRAP.Focus()
    'End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        If (Val(TextBoxSCRAPINNER.Text) = Val(TextBoxINNERLOTE.Text)) And (Val(TextBoxSCRAPPOUTER.SelectedText) = Val(TextBoxOUTTERLOTE.Text)) Then
            NO_SIRVE_INNER = TextBoxSCRAPINNER.Text
            NO_SIRVE_OUTER = TextBoxSCRAPPOUTER.Text
            Me.Close()
            frm_prod.Show()
            escribirlote()
        Else
            escribirlote()
        End If
        TextBoxSCRAP.Focus()
    End Sub
End Class