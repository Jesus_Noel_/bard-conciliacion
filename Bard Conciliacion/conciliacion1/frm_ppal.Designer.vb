﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_ppal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ButtonSalir = New System.Windows.Forms.Button()
        Me.ButtonMantenimiento = New System.Windows.Forms.Button()
        Me.ButtonImprimirReporte = New System.Windows.Forms.Button()
        Me.ButtonProduccion = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Gray
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(-30, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(573, 48)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "MENU"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Panel1
        '
        Me.Panel1.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.Update_Color_BD_PNG_Logo
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Panel1.Location = New System.Drawing.Point(638, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(267, 125)
        Me.Panel1.TabIndex = 5
        '
        'ButtonSalir
        '
        Me.ButtonSalir.BackColor = System.Drawing.Color.Transparent
        Me.ButtonSalir.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.SALIR
        Me.ButtonSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonSalir.FlatAppearance.BorderSize = 0
        Me.ButtonSalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.ButtonSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.ButtonSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSalir.Location = New System.Drawing.Point(685, 515)
        Me.ButtonSalir.Name = "ButtonSalir"
        Me.ButtonSalir.Size = New System.Drawing.Size(194, 72)
        Me.ButtonSalir.TabIndex = 4
        Me.ButtonSalir.UseVisualStyleBackColor = False
        '
        'ButtonMantenimiento
        '
        Me.ButtonMantenimiento.BackColor = System.Drawing.Color.Transparent
        Me.ButtonMantenimiento.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.MANTENIMIENTO1
        Me.ButtonMantenimiento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonMantenimiento.FlatAppearance.BorderSize = 0
        Me.ButtonMantenimiento.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent
        Me.ButtonMantenimiento.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.ButtonMantenimiento.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.ButtonMantenimiento.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonMantenimiento.Font = New System.Drawing.Font("Times New Roman", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonMantenimiento.Location = New System.Drawing.Point(282, 249)
        Me.ButtonMantenimiento.Name = "ButtonMantenimiento"
        Me.ButtonMantenimiento.Size = New System.Drawing.Size(332, 95)
        Me.ButtonMantenimiento.TabIndex = 3
        Me.ButtonMantenimiento.UseVisualStyleBackColor = False
        '
        'ButtonImprimirReporte
        '
        Me.ButtonImprimirReporte.BackColor = System.Drawing.Color.Transparent
        Me.ButtonImprimirReporte.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.IMPRIMIR_REPORTE1
        Me.ButtonImprimirReporte.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonImprimirReporte.Enabled = False
        Me.ButtonImprimirReporte.FlatAppearance.BorderSize = 0
        Me.ButtonImprimirReporte.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent
        Me.ButtonImprimirReporte.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.ButtonImprimirReporte.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.ButtonImprimirReporte.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonImprimirReporte.Font = New System.Drawing.Font("Times New Roman", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonImprimirReporte.Location = New System.Drawing.Point(282, 368)
        Me.ButtonImprimirReporte.Name = "ButtonImprimirReporte"
        Me.ButtonImprimirReporte.Size = New System.Drawing.Size(332, 95)
        Me.ButtonImprimirReporte.TabIndex = 2
        Me.ButtonImprimirReporte.UseVisualStyleBackColor = False
        Me.ButtonImprimirReporte.Visible = False
        '
        'ButtonProduccion
        '
        Me.ButtonProduccion.BackColor = System.Drawing.Color.Transparent
        Me.ButtonProduccion.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.PRODUCCION1
        Me.ButtonProduccion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonProduccion.FlatAppearance.BorderSize = 0
        Me.ButtonProduccion.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent
        Me.ButtonProduccion.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.ButtonProduccion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.ButtonProduccion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonProduccion.Font = New System.Drawing.Font("Times New Roman", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonProduccion.Location = New System.Drawing.Point(282, 132)
        Me.ButtonProduccion.Name = "ButtonProduccion"
        Me.ButtonProduccion.Size = New System.Drawing.Size(332, 95)
        Me.ButtonProduccion.TabIndex = 0
        Me.ButtonProduccion.UseVisualStyleBackColor = False
        '
        'frm_ppal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(917, 620)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ButtonSalir)
        Me.Controls.Add(Me.ButtonMantenimiento)
        Me.Controls.Add(Me.ButtonImprimirReporte)
        Me.Controls.Add(Me.ButtonProduccion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_ppal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "  "
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ButtonProduccion As System.Windows.Forms.Button
    Friend WithEvents ButtonImprimirReporte As System.Windows.Forms.Button
    Friend WithEvents ButtonMantenimiento As System.Windows.Forms.Button
    Friend WithEvents ButtonSalir As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label

End Class
