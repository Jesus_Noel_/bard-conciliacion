﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FORM_SCRAP
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.TextBoxTOTALSCRAP = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBoxSCRAPPOUTER = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBoxSCRAPINNER = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBoxOUTTERLOTE = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBoxINNERLOTE = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TextBoxSCRAP = New System.Windows.Forms.TextBox()
        Me.TextBoxOuters = New System.Windows.Forms.TextBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.ButtonRegresar = New System.Windows.Forms.Button()
        Me.ButtonAceptar = New System.Windows.Forms.Button()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.TextBoxTOTALSCRAP)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Controls.Add(Me.TextBoxSCRAPPOUTER)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.TextBoxSCRAPINNER)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.White
        Me.GroupBox3.Location = New System.Drawing.Point(396, 176)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(269, 205)
        Me.GroupBox3.TabIndex = 59
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "SCRAP"
        '
        'TextBoxTOTALSCRAP
        '
        Me.TextBoxTOTALSCRAP.BackColor = System.Drawing.Color.White
        Me.TextBoxTOTALSCRAP.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxTOTALSCRAP.Location = New System.Drawing.Point(123, 133)
        Me.TextBoxTOTALSCRAP.Name = "TextBoxTOTALSCRAP"
        Me.TextBoxTOTALSCRAP.ReadOnly = True
        Me.TextBoxTOTALSCRAP.Size = New System.Drawing.Size(119, 32)
        Me.TextBoxTOTALSCRAP.TabIndex = 37
        Me.TextBoxTOTALSCRAP.Text = "0"
        Me.TextBoxTOTALSCRAP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(21, 141)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 24)
        Me.Label1.TabIndex = 36
        Me.Label1.Text = "TOTAL"
        '
        'TextBoxSCRAPPOUTER
        '
        Me.TextBoxSCRAPPOUTER.BackColor = System.Drawing.Color.White
        Me.TextBoxSCRAPPOUTER.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxSCRAPPOUTER.Location = New System.Drawing.Point(123, 83)
        Me.TextBoxSCRAPPOUTER.Name = "TextBoxSCRAPPOUTER"
        Me.TextBoxSCRAPPOUTER.ReadOnly = True
        Me.TextBoxSCRAPPOUTER.Size = New System.Drawing.Size(119, 32)
        Me.TextBoxSCRAPPOUTER.TabIndex = 28
        Me.TextBoxSCRAPPOUTER.Text = "0"
        Me.TextBoxSCRAPPOUTER.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(21, 91)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 24)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "OUTER"
        '
        'TextBoxSCRAPINNER
        '
        Me.TextBoxSCRAPINNER.BackColor = System.Drawing.Color.White
        Me.TextBoxSCRAPINNER.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxSCRAPINNER.Location = New System.Drawing.Point(123, 40)
        Me.TextBoxSCRAPINNER.Name = "TextBoxSCRAPINNER"
        Me.TextBoxSCRAPINNER.ReadOnly = True
        Me.TextBoxSCRAPINNER.Size = New System.Drawing.Size(119, 32)
        Me.TextBoxSCRAPINNER.TabIndex = 26
        Me.TextBoxSCRAPINNER.Text = "0"
        Me.TextBoxSCRAPINNER.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(21, 49)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 24)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "INNER"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.TextBoxOUTTERLOTE)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.TextBoxINNERLOTE)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(41, 176)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(319, 205)
        Me.GroupBox1.TabIndex = 56
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "DATOS LOTE"
        '
        'TextBoxOUTTERLOTE
        '
        Me.TextBoxOUTTERLOTE.BackColor = System.Drawing.Color.White
        Me.TextBoxOUTTERLOTE.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxOUTTERLOTE.Location = New System.Drawing.Point(161, 123)
        Me.TextBoxOUTTERLOTE.Name = "TextBoxOUTTERLOTE"
        Me.TextBoxOUTTERLOTE.ReadOnly = True
        Me.TextBoxOUTTERLOTE.Size = New System.Drawing.Size(129, 32)
        Me.TextBoxOUTTERLOTE.TabIndex = 19
        Me.TextBoxOUTTERLOTE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(30, 123)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(83, 24)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "OUTER"
        '
        'TextBoxINNERLOTE
        '
        Me.TextBoxINNERLOTE.BackColor = System.Drawing.Color.White
        Me.TextBoxINNERLOTE.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxINNERLOTE.Location = New System.Drawing.Point(161, 49)
        Me.TextBoxINNERLOTE.Name = "TextBoxINNERLOTE"
        Me.TextBoxINNERLOTE.ReadOnly = True
        Me.TextBoxINNERLOTE.Size = New System.Drawing.Size(129, 32)
        Me.TextBoxINNERLOTE.TabIndex = 17
        Me.TextBoxINNERLOTE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(30, 55)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 24)
        Me.Label5.TabIndex = 16
        Me.Label5.Text = "INNER"
        '
        'Panel3
        '
        Me.Panel3.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.FONDO_1
        Me.Panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel3.Controls.Add(Me.Label15)
        Me.Panel3.Location = New System.Drawing.Point(139, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(476, 166)
        Me.Panel3.TabIndex = 60
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Yellow
        Me.Label15.Location = New System.Drawing.Point(86, 34)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(324, 77)
        Me.Label15.TabIndex = 42
        Me.Label15.Text = "INFORMACION DE SCRAP"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBoxSCRAP
        '
        Me.TextBoxSCRAP.AcceptsReturn = True
        Me.TextBoxSCRAP.BackColor = System.Drawing.Color.White
        Me.TextBoxSCRAP.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxSCRAP.Location = New System.Drawing.Point(304, 420)
        Me.TextBoxSCRAP.Name = "TextBoxSCRAP"
        Me.TextBoxSCRAP.Size = New System.Drawing.Size(369, 32)
        Me.TextBoxSCRAP.TabIndex = 74
        Me.TextBoxSCRAP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBoxOuters
        '
        Me.TextBoxOuters.AcceptsReturn = True
        Me.TextBoxOuters.BackColor = System.Drawing.Color.White
        Me.TextBoxOuters.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxOuters.Location = New System.Drawing.Point(304, 420)
        Me.TextBoxOuters.Name = "TextBoxOuters"
        Me.TextBoxOuters.Size = New System.Drawing.Size(369, 32)
        Me.TextBoxOuters.TabIndex = 73
        Me.TextBoxOuters.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBoxOuters.Visible = False
        '
        'Panel5
        '
        Me.Panel5.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.escaner
        Me.Panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Panel5.Location = New System.Drawing.Point(55, 404)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(221, 54)
        Me.Panel5.TabIndex = 72
        '
        'TextBox6
        '
        Me.TextBox6.AcceptsReturn = True
        Me.TextBox6.BackColor = System.Drawing.Color.White
        Me.TextBox6.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox6.Location = New System.Drawing.Point(304, 420)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(369, 32)
        Me.TextBox6.TabIndex = 71
        Me.TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ButtonRegresar
        '
        Me.ButtonRegresar.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ButtonRegresar.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.REGRRESAR_E
        Me.ButtonRegresar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonRegresar.FlatAppearance.BorderSize = 0
        Me.ButtonRegresar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonRegresar.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonRegresar.Location = New System.Drawing.Point(341, 487)
        Me.ButtonRegresar.Name = "ButtonRegresar"
        Me.ButtonRegresar.Size = New System.Drawing.Size(200, 55)
        Me.ButtonRegresar.TabIndex = 76
        Me.ButtonRegresar.UseVisualStyleBackColor = False
        '
        'ButtonAceptar
        '
        Me.ButtonAceptar.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ButtonAceptar.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.ACEPTAR
        Me.ButtonAceptar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonAceptar.FlatAppearance.BorderSize = 0
        Me.ButtonAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonAceptar.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAceptar.Location = New System.Drawing.Point(114, 487)
        Me.ButtonAceptar.Name = "ButtonAceptar"
        Me.ButtonAceptar.Size = New System.Drawing.Size(231, 55)
        Me.ButtonAceptar.TabIndex = 75
        Me.ButtonAceptar.UseVisualStyleBackColor = False
        '
        'TextBox7
        '
        Me.TextBox7.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(139, 499)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(187, 32)
        Me.TextBox7.TabIndex = 77
        Me.TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Timer1
        '
        '
        'Timer2
        '
        '
        'FORM_SCRAP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(738, 556)
        Me.ControlBox = False
        Me.Controls.Add(Me.ButtonRegresar)
        Me.Controls.Add(Me.ButtonAceptar)
        Me.Controls.Add(Me.TextBox7)
        Me.Controls.Add(Me.TextBoxSCRAP)
        Me.Controls.Add(Me.TextBoxOuters)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.TextBox6)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.ForeColor = System.Drawing.Color.White
        Me.Name = "FORM_SCRAP"
        Me.Text = "FORM_SCRAP"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxTOTALSCRAP As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBoxSCRAPPOUTER As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBoxSCRAPINNER As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxOUTTERLOTE As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBoxINNERLOTE As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TextBoxSCRAP As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxOuters As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents ButtonRegresar As System.Windows.Forms.Button
    Friend WithEvents ButtonAceptar As System.Windows.Forms.Button
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
End Class
