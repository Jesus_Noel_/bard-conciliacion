﻿Imports System.Data
Imports System.Data.OleDb
Imports Excel = Microsoft.Office.Interop.Excel
Imports Interop = Microsoft.Office.Interop.Excel
'Imports CrystalDecisions.CrystalReports.Engine

Module Module1
    Public CADENA As String
    Public FALTAN_INNER As Integer
    Public PKS As String
    Public FALTAN_OUTER As Integer
    Public LOTE_SCRAP As String
    Public lote_PROD As String
    Public cajasinternasXexterna As Integer
    Public ETIQUETA_NO_LEGIBLE_INNER As Integer
    Public ETIQUETA_NO_LEGIBLE As Integer
    Public ETIQUETA_NO_LEGIBLE_OUTER As Integer
    Public IDConciliar As Integer
    Public scrap_produccion As Boolean = False
    Public NO_LEGIBLE As Integer
    Public INNER_SOBRANES As Integer
    Public TOTAL_INNER_SOBRANTES As Integer
    Public TOTAL_ETIQUETASINNER_SOBRANTES As Integer
    Public TOTAL_ETIQUETASOUTER_SOBRANTES As Integer
    Public SCRAP_INNER_SOBRANTE As Boolean
    Public ETIQUETAS_SOBRANTES_INNER As Boolean
    Public ETIQUETAS_SOBRANTES_OUTER As Boolean
    Public FALTANOUTER As Boolean = False
    Public mantennimiento As Boolean = False
    Public Escan_codigohw As String = ""
    Public scanner_com As String
    Public NOMBRE_USUARIO As String
    Public bypass As Boolean
    Public CNN As OleDbConnection
    Public COMD As OleDbCommand
    Public ADPTR As OleDbDataAdapter
    Public DTSET As DataSet
    Public DTROW As DataRow
    Public DREAD As OleDbDataReader
    Public Query As String
    Dim consulta As String
    Public Ruta_BD As String = Application.StartupPath & "\datos.accdb;"
    'Public ruta_scanner As String = Application.StartupPath & "\config_scanner.sde"
    Public archiexcel As String = Application.StartupPath & "\archivopks.csv"
    Public Sub BD_AbrirConexion()
        CNN = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Ruta_BD & "Persist Security Info=False;")

        Try
            CNN.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Ruta_BD & "Persist Security Info=False;"
            CNN.Open()
        Catch ex As Exception
            MsgBox("Error", vbCritical, "Sin Conexion a Base de Datos")
            End
        End Try
    End Sub
    Public Sub inicializa_base()

        Dim i As Integer
        BD_AbrirConexion()
        consulta = "SELECT * From tblUsuarios "
        Dim cmd As OleDbCommand = New OleDbCommand(consulta, CNN)

        dr = cmd.ExecuteReader
        i = 0
        While dr.Read()
            usuarios(i).user = dr("nombre").ToString
            usuarios(i).pass = dr("clave").ToString
            usuarios(i).level = dr("nivel").ToString
            usuarios(i).reloj = dr("Numero_reloj").ToString
            'usuarios(i).fechaini = dr("Fecha_Inicial").ToString
            'usuarios(i).fechafin = dr("Fecha_Final").ToString
            i = i + 1
        End While

        CNN.Close()

    End Sub
    Public Sub inicializa_baseCHECKLIST()

        Dim i As Integer
        BD_AbrirConexion()
        consulta = "SELECT * From tblUsuarios "
        Dim cmd As OleDbCommand = New OleDbCommand(consulta, CNN)

        dr = cmd.ExecuteReader
        i = 0
        While dr.Read()
            usuarios(i).user = dr("nombre").ToString
            usuarios(i).reloj = dr("Numero_reloj").ToString
            usuarios(i).level = dr("nivel").ToString
           
            i = i + 1
        End While

        CNN.Close()

    End Sub
    Public Sub buscalote()
        Dim consulta As String
        Dim i As Integer
        'Dim lo As String
        BD_AbrirConexion()
        consulta = "SELECT * From tblLotes  WHERE Lote='" & IDLote & "'"
        Dim cmd As OleDbCommand = New OleDbCommand(consulta, CNN)

        dr = cmd.ExecuteReader
        i = 0

        While dr.Read()
            IDLote = dr("Lote").ToString
            Modelo = dr("Modelo").ToString
            statuslote = dr("Status").ToString
            loteencontrado = True
        End While

        CNN.Close()

    End Sub
    
    Public Sub DATOS_DE_LOTE()
        Dim currentFind As Interop.Range = Nothing
        Dim CONTENIDO As String
        CONTENIDO = My.Computer.FileSystem.ReadAllText(Application.StartupPath & "\archivopks.csv") 'C:\Users\Brenda\OneDrive\BARD\MODIFICADO MAYO 2015 - Copy\conciliacion1\bin
        Dim registros() As String = Split(CONTENIDO, vbCrLf)
        Dim LOTE As String
        LOTE = Frm_datos_Lote.TextBox1.Text
        For i = 0 To registros.Length - 1
            If Len(registros(i)) > 15 Then
                campos = Split(registros(i), ",")
                If campos(0) = LOTE Then
                    Frm_datos_Lote.TextBox2.Text = campos(1).ToString
                    Frm_datos_Lote.TextBox3.Text = campos(2).ToString
                    PKInn = campos(3).ToString
                    PKOut = campos(4).ToString
                    Dim CAN_CAMPOS As Integer = campos.Length - 3
                    PKS = campos(3)
                    Try
                        For XX As Integer = 1 To (CAN_CAMPOS) - 1
                            PKS = PKS & vbCrLf & campos(XX + 3)
                            'PKS = campos(3).ToString & vbCrLf & campos(4).ToString & vbCrLf & campos(5).ToString & vbCrLf & campos(6).ToString & vbCrLf & campos(7).ToString & vbCrLf & campos(8).ToString & vbCrLf & campos(9).ToString
                        Next
                    Catch ex As Exception

                    End Try
                    
                    i = registros.Length
                    loteenCSV = True
                Else
                    loteenCSV = False
                End If
            End If
Next     
    End Sub
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try

    End Sub
    Public Sub leerloteconciliando()
        Dim consulta As String
        Dim i As Integer

        BD_AbrirConexion()
        consulta = "SELECT * From tblConciliar  WHERE Lote='" & IDLote & "'"
        Dim cmd As OleDbCommand = New OleDbCommand(consulta, CNN)

        dr = cmd.ExecuteReader
        i = 0

        dr.Read()
        ' IDConciliar = dr("IDConciliar").ToString
        If dr.HasRows Then
            Meta = dr("Meta").ToString
            Estacion = dr("Estacion").ToString
            ActCajasInternas = dr("ActCajasInternas").ToString
            ActCajasExternas = dr("ActCajasExternas").ToString
            ExactCajasInternas = dr("ExactCajasInternas").ToString
            ExactCajasExternas = dr("ExactCajasExternas").ToString
            NO_SIRVE_INNER = dr("Scrap_Interno").ToString
            NO_SIRVE_OUTER = dr("Scrap_Externo").ToString
            PIEZAS_POR_CAJA = dr("PzXInner").ToString
            cajasinternasXexterna = dr("InnerXOutter").ToString
            ToNext = dr("ToNext").ToString
        End If
    
        CNN.Close()

    End Sub
    Public Sub DatosComentario()
        Dim consulta As String
        Dim i As Integer

        BD_AbrirConexion()
        consulta = "SELECT * From tblConciliar  WHERE Lote='" & IDLote & "'"
        Dim cmd As OleDbCommand = New OleDbCommand(consulta, CNN)

        dr = cmd.ExecuteReader
        i = 0

        dr.Read()
        IDConciliar = dr("IDConciliar").ToString
        Meta = dr("Meta").ToString
        Estacion = dr("Estacion").ToString
        ActCajasInternas = dr("ActCajasInternas").ToString
        ActCajasExternas = dr("ActCajasExternas").ToString
        ExactCajasInternas = dr("ExactCajasInternas").ToString
        ExactCajasExternas = dr("ExactCajasExternas").ToString
        NO_SIRVE_INNER = dr("Scrap_Interno").ToString
        NO_SIRVE_OUTER = dr("Scrap_Externo").ToString
        CNN.Close()

    End Sub
    Public Sub escrbirlotenuevo()
        Dim consulta As String
        Dim i, icount As Integer
        Dim fecha As Date
        Dim esta As String
        Dim cmd As New OleDbCommand
        fecha = Date.Today

        BD_AbrirConexion()


        consulta = "INSERT INTO tblLotes (Lote,Modelo,Status,Num_Emple_Inicio,Fecha_Ini) VALUES ('" & _
                     frm_prod.LabelLOTE.Text & "','" & frm_prod.LabelMODELO.Text & "','" & statuslote & "','" & _
                     frm_prod.Label22.Text & "','" & fecha & "')"
        cmd = New OleDbCommand(consulta, CNN)
        icount = cmd.ExecuteNonQuery

        i = 0
        esta = 1
        consulta = "INSERT INTO tblConciliar (Lote,Estacion,Meta,ActCajasInternas,ActCajasExternas," & _
                      "ExactCajasInternas,ExactCajasExternas,UsuarioInicial,PzXInner ) VALUES ('" & _
                      frm_prod.LabelLOTE.Text & "','" & esta & "','" & Meta & "','" & _
                      ActCajasInternas & "','" & ActCajasExternas & "','" & _
                       ExactCajasInternas & "','" & ExactCajasExternas & "','" & Usuarioinicial & "','" & PIEZAS_POR_CAJA & "')"

        cmd = New OleDbCommand(consulta, CNN)
        icount = cmd.ExecuteNonQuery

        CNN.Close()

    End Sub
    Public Sub escribirlote()
        Dim consulta As String
        Dim icount As Integer
        Dim fecha As Date
        Dim Esta_conciliado As String


        Dim cmd As New OleDbCommand


        If conciliando = True Then
            Esta_conciliado = "SI"
        Else
            Esta_conciliado = "NO"
        End If
        fecha = Date.Today

        BD_AbrirConexion()
        consulta = "UPDATE tblLotes SET Status='" & statuslote & "', Num_Emple_Sus='" & relojUsuario & "', Fecha_Sus='" & fecha & _
                    "' WHERE Lote='" & frm_prod.LabelLOTE.Text & "'"


        cmd = New OleDbCommand(consulta, CNN)
        icount = cmd.ExecuteNonQuery

        consulta = "UPDATE tblConciliar SET Estacion= '" & Estacion & "', ActCajasInternas='" & Val(frm_prod.TextBoxINNERSCAN.Text) & "', ActCajasExternas='" & Val(frm_prod.TextBoxOUTERSCAN.Text) & _
            "', ExactCajasInternas='" & Val(frm_prod.TextBoxINERLOTE.Text) & "', ExactCajasExternas='" & Val(frm_prod.TextBoxOUTERLOTE.Text) & "', Scrap_Interno='" & Val(frm_prod.TextBoxINNERSCRAP.Text) & "', Scrap_Externo='" & Val(frm_prod.TextBoxOUTERSCRAP.Text) & "', PzXInner='" & PIEZAS_POR_CAJA & "', InnerXOutter='" & cajasinternasXexterna & "', ToNext='" & ToNext & "', Conciliado='" & Esta_conciliado & "'  WHERE Lote='" & frm_prod.LabelLOTE.Text & "'"
        cmd = New OleDbCommand(consulta, CNN)
        icount = cmd.ExecuteNonQuery
        CNN.Close()
    End Sub

    Public Sub ACTUALIZAR_LOTECONCILIADO()
        Dim consulta As String
        Dim icount As Integer
        Dim fecha As Date
        Dim interna As Integer
        Dim externa As Integer
        Dim conciliar_loteterminado As String

        Dim cmd As New OleDbCommand
        fecha = Date.Today

        BD_AbrirConexion()
        consulta = "UPDATE tblLotes SET Status='" & statuslote & "', Num_Emple_Termi='" & relojUsuario & "', Fecha_Fin='" & fecha & _
                    "' WHERE Lote='" & frm_prod.LabelLOTE.Text & "'"


        cmd = New OleDbCommand(consulta, CNN)
        icount = cmd.ExecuteNonQuery

        interna = Val(frm_prod.TextBoxINNERSCAN.Text)
        externa = Val(frm_prod.TextBoxOUTERSCAN.Text)
        conciliar_loteterminado = VALOR_CONCILIAR

        consulta = "UPDATE tblConciliar SET Estacion= '" & Estacion & "', ActCajasInternas='" & frm_prod.TextBoxINNERSCAN.Text & "', ActCajasExternas='" & frm_prod.TextBoxOUTERSCAN.Text & _
            "', ExactCajasInternas='" & interna & "', ExactCajasExternas='" & externa & "', Scrap_Interno='" & Val(frm_prod.TextBoxINNERSCRAP.Text) & "', UsuarioConciliado='" & relojUsuario & "'  WHERE Lote='" & frm_prod.LabelLOTE.Text & "'"
        cmd = New OleDbCommand(consulta, CNN)
        icount = cmd.ExecuteNonQuery
        CNN.Close()
    End Sub
    Public Sub AGREGAR_COMENTARIO()
        Dim consulta As String
        Dim icount As Integer
        Dim fecha As Date



        Dim cmd As New OleDbCommand
        fecha = Date.Today

        BD_AbrirConexion()

        consulta = "INSERT INTO  tblComentarios (IDConciliar,IDUsuario,tipoComentario,Comentario,FechaHora,ActCajasInternas,ActCajasExternas,Lote) VALUES ('" & _
            IDConciliar & "','" & FrmComentarios.Label2.Text & "','" & 1 & "','" & FrmComentarios.TextBox1.Text & "','" & _
  fecha & "','" & frm_prod.TextBoxINNERSCAN.Text & "','" & frm_prod.TextBoxOUTERSCAN.Text & "','" & frm_prod.LabelLOTE.Text & "')"

        cmd = New OleDbCommand(consulta, CNN)
        icount = cmd.ExecuteNonQuery
        MsgBox("COMENTARIO GUARDADO CORECTAMENTE", vbOK)
        CNN.Close()

    End Sub
    Public Sub ACTUALIZAR_COMENTARIO()
        Dim consulta As String
        Dim icount As Integer
        Dim fecha As Date
        Dim interna As Integer
        Dim externa As Integer
        Dim conciliar_loteterminado As String
        Dim comentario_Final As String
        comentario_Final = Comentario_Ant + ";" + Comentario
        Dim cmd As New OleDbCommand
        fecha = Date.Today

        BD_AbrirConexion()
        consulta = "UPDATE tblComentarios SET Comentario='" & comentario_Final & _
                    "' WHERE Lote='" & frm_prod.LabelLOTE.Text & "'"


        cmd = New OleDbCommand(consulta, CNN)
        icount = cmd.ExecuteNonQuery

       
        CNN.Close()
    End Sub
    Public Sub Buscar_COMENTARIO()
        Dim consulta As String
        Dim i As Integer

        BD_AbrirConexion()
        consulta = "SELECT Comentario From tblComentarios  WHERE Lote='" & frm_prod.LabelLOTE.Text & "'"
        Dim cmd As OleDbCommand = New OleDbCommand(consulta, CNN)

        dr = cmd.ExecuteReader
        i = 0

        dr.Read()
        If dr.HasRows = True Then
            Comentario_DeBD = dr("Comentario").ToString
        Else
            Comentario_DeBD = ""
        End If

        CNN.Close()

    End Sub
    Public Sub GENERAR_REPORTE()
        Dim DATOS As New Class1()
        DATOS.ActCajasExternasREPORTE = ActCajasExternas
        DATOS.ActCajasInternas_REPORTE = ActCajasInternas
        DATOS.Comentario_REPORTE = FrmComentarios.TextBox1.Text
        DATOS.Estacion_REPORTE = Estacion
        DATOS.ExactCajasExternas_REPORTE = ExactCajasExternas
        DATOS.ExactCajasInternas_REPORTE = ExactCajasInternas
        DATOS.Fecha_Fin_REPORTE = Date.Today
        DATOS.Fecha_Ini_REPORTE = Date.Today
        DATOS.Lote_REPORTE = LoteNum_Rep
        DATOS.Meta_REPORTE = Meta
        DATOS.Nombre_REPORTE = Usuarioinicial
        DATOS.Num_Emple_inicio_REPORTE = relojUsuario
        DATOS.Num_Emple_Termi_REPORTE = UsuarioConciliado
        ' DATOS.Scrap_Externo_REPORTE = frm_prod.TextBoxOUTERSCRAP.Text

        'DATOS.Scrap_Interno_REPORTE = frm_prod.TextBoxINNERSCRAP.Text
        DATOS.Scrap_Externo_REPORTE = NO_SIRVE_OUTER
        DATOS.Scrap_Interno_REPORTE = NO_SIRVE_INNER
        DATOS.UsuarioInicial_REPORTE = usuario_Inicial_Rep
        DATOS.UsuarioConciliado_REPORTE = "NOMBRE"
        DATOS.Modelo_reporte = Modelo
        DATOS.PKS_REPORTES = PKS
        DATOS.Meta_REPORTE = sum_Inner
        DATOS.Status_REPORTE = Sum_Outer
        DATOS.Comentario_REPORTE = Comentario_DeBD

        DATOS.ActPiezasTotales_REPORTE = frm_prod.TextBoxPIEZASSCAN.Text
        'DATOS.TotalInners_REPORTE = DATOS.Scrap_Interno_REPORTE + DATOS.ActCajasInternas_REPORTE
        ' DATOS.TotalOuters_REPORTE = DATOS.Scrap_Externo_REPORTE + DATOS.ActCajasExternasREPORTE
        '  DATOS.
        Dim FRM As New FormReps()
        FRM.Detail.Add(DATOS)
        FRM.Show()

    End Sub
   

    Public Sub Actualizar_CSV()
        Dim sSource As String
        Dim sTarget As String
        sSource = Path_BD_Red
        ' sSource = "Z:\archivopks.csv"

        sTarget = Application.StartupPath & "\archivopks.csv"
       
        Dim FileToCopy As String
        Dim NewCopy As String

     
        'If System.IO.File.Exists(sTarget) = True Then
        Try
            System.IO.File.Copy(sSource, sTarget, True)

        Catch ex As Exception
            MsgBox("Error al conectar con al BD")
        End Try
       

        'End If

    End Sub
    Public Sub Buscar_Path()
        Dim i As Integer
        Dim consulta As String
        Path_BD_Red = ""
        BD_AbrirConexion()
        consulta = "SELECT Path From Path  WHERE ID='0'"
        Dim cmd As OleDbCommand = New OleDbCommand(consulta, CNN)

        dr = cmd.ExecuteReader
        i = 0

        dr.Read()
        If dr.HasRows = True Then
            If dr("Path").ToString <> "" Then
                Path_BD_Red = dr("Path").ToString
            Else
                MsgBox("Asigne una direccion para la Base de Datos")
            End If

        Else
            MsgBox("Asigne una direccion para la Base de Datos")
            Path_BD_Red = ""
        End If
        CNN.Close()

    End Sub
    Public Sub Modificar_Path()
        Dim consulta As String
        Dim icount As Integer
        

        Dim cmd As New OleDbCommand


        BD_AbrirConexion()


        consulta = "UPDATE Path SET Path='" & New_Path_BD_Red & _
                    "' WHERE ID='0'"


        cmd = New OleDbCommand(consulta, CNN)
        Try
            icount = cmd.ExecuteNonQuery
            MsgBox("Direccion de Base de Datos Actualizada.")
        Catch ex As Exception
            MsgBox(ex.ToString)

        End Try



        CNN.Close()

    End Sub
End Module
