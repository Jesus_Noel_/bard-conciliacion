﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_prod
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_prod))
        Me.TextBoxINERLOTE = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBoxOUTERLOTE = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBoxOUTERSCAN = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBoxINNERSCAN = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TextBoXTOTALPIEZALOTE = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextBoxPIEZASSCAN = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Refresh_DB_Timer = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ButtonTerminarScrap = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.TextBoxTOTALSCRAP = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBoxOUTERSCRAP = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TextBoxINNERSCRAP = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.ButtoncCODIGONOLEGIBLE = New System.Windows.Forms.Button()
        Me.TextBoxSCRAP = New System.Windows.Forms.TextBox()
        Me.TextBoxOuters = New System.Windows.Forms.TextBox()
        Me.ButtonSCRAP = New System.Windows.Forms.Button()
        Me.ButtonCOMENTARIO = New System.Windows.Forms.Button()
        Me.ButtonSUSPENDER = New System.Windows.Forms.Button()
        Me.ButtonConciliar = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.ButtonCAMBIOUSUARIO = New System.Windows.Forms.Button()
        Me.ButtonREGRESAR = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.LabelLOTE = New System.Windows.Forms.Label()
        Me.LabelMODELO = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ButtonACEPTAR = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.Panel1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TextBoxINERLOTE
        '
        Me.TextBoxINERLOTE.BackColor = System.Drawing.Color.White
        Me.TextBoxINERLOTE.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxINERLOTE.Location = New System.Drawing.Point(161, 40)
        Me.TextBoxINERLOTE.Name = "TextBoxINERLOTE"
        Me.TextBoxINERLOTE.ReadOnly = True
        Me.TextBoxINERLOTE.Size = New System.Drawing.Size(129, 32)
        Me.TextBoxINERLOTE.TabIndex = 17
        Me.TextBoxINERLOTE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(30, 46)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 24)
        Me.Label5.TabIndex = 16
        Me.Label5.Text = "INNER"
        '
        'TextBoxOUTERLOTE
        '
        Me.TextBoxOUTERLOTE.BackColor = System.Drawing.Color.White
        Me.TextBoxOUTERLOTE.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxOUTERLOTE.Location = New System.Drawing.Point(161, 91)
        Me.TextBoxOUTERLOTE.Name = "TextBoxOUTERLOTE"
        Me.TextBoxOUTERLOTE.ReadOnly = True
        Me.TextBoxOUTERLOTE.Size = New System.Drawing.Size(129, 32)
        Me.TextBoxOUTERLOTE.TabIndex = 19
        Me.TextBoxOUTERLOTE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(30, 91)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(83, 24)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "OUTER"
        '
        'TextBoxOUTERSCAN
        '
        Me.TextBoxOUTERSCAN.BackColor = System.Drawing.Color.White
        Me.TextBoxOUTERSCAN.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxOUTERSCAN.Location = New System.Drawing.Point(152, 91)
        Me.TextBoxOUTERSCAN.Name = "TextBoxOUTERSCAN"
        Me.TextBoxOUTERSCAN.ReadOnly = True
        Me.TextBoxOUTERSCAN.Size = New System.Drawing.Size(119, 32)
        Me.TextBoxOUTERSCAN.TabIndex = 28
        Me.TextBoxOUTERSCAN.Text = "0"
        Me.TextBoxOUTERSCAN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(28, 91)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(83, 24)
        Me.Label11.TabIndex = 27
        Me.Label11.Text = "OUTER"
        '
        'TextBoxINNERSCAN
        '
        Me.TextBoxINNERSCAN.BackColor = System.Drawing.Color.White
        Me.TextBoxINNERSCAN.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxINNERSCAN.Location = New System.Drawing.Point(152, 44)
        Me.TextBoxINNERSCAN.Name = "TextBoxINNERSCAN"
        Me.TextBoxINNERSCAN.ReadOnly = True
        Me.TextBoxINNERSCAN.Size = New System.Drawing.Size(119, 32)
        Me.TextBoxINNERSCAN.TabIndex = 26
        Me.TextBoxINNERSCAN.Text = "0"
        Me.TextBoxINNERSCAN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(28, 46)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(76, 24)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "INNER"
        '
        'TextBoXTOTALPIEZALOTE
        '
        Me.TextBoXTOTALPIEZALOTE.BackColor = System.Drawing.Color.White
        Me.TextBoXTOTALPIEZALOTE.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoXTOTALPIEZALOTE.Location = New System.Drawing.Point(161, 149)
        Me.TextBoXTOTALPIEZALOTE.Name = "TextBoXTOTALPIEZALOTE"
        Me.TextBoXTOTALPIEZALOTE.ReadOnly = True
        Me.TextBoXTOTALPIEZALOTE.Size = New System.Drawing.Size(129, 32)
        Me.TextBoXTOTALPIEZALOTE.TabIndex = 35
        Me.TextBoXTOTALPIEZALOTE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(30, 133)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(117, 48)
        Me.Label13.TabIndex = 34
        Me.Label13.Text = "TOTAL DE " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  PIEZAS "
        '
        'TextBoxPIEZASSCAN
        '
        Me.TextBoxPIEZASSCAN.BackColor = System.Drawing.Color.White
        Me.TextBoxPIEZASSCAN.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxPIEZASSCAN.Location = New System.Drawing.Point(152, 149)
        Me.TextBoxPIEZASSCAN.Name = "TextBoxPIEZASSCAN"
        Me.TextBoxPIEZASSCAN.ReadOnly = True
        Me.TextBoxPIEZASSCAN.Size = New System.Drawing.Size(119, 32)
        Me.TextBoxPIEZASSCAN.TabIndex = 37
        Me.TextBoxPIEZASSCAN.Text = "0"
        Me.TextBoxPIEZASSCAN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(28, 133)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(117, 48)
        Me.Label14.TabIndex = 36
        Me.Label14.Text = "TOTAL DE " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  PIEZAS"
        '
        'Refresh_DB_Timer
        '
        Me.Refresh_DB_Timer.Enabled = True
        Me.Refresh_DB_Timer.Interval = 1000
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.ButtonTerminarScrap)
        Me.Panel1.Controls.Add(Me.GroupBox3)
        Me.Panel1.Controls.Add(Me.ButtoncCODIGONOLEGIBLE)
        Me.Panel1.Controls.Add(Me.TextBoxSCRAP)
        Me.Panel1.Controls.Add(Me.TextBoxOuters)
        Me.Panel1.Controls.Add(Me.ButtonSCRAP)
        Me.Panel1.Controls.Add(Me.ButtonCOMENTARIO)
        Me.Panel1.Controls.Add(Me.ButtonSUSPENDER)
        Me.Panel1.Controls.Add(Me.ButtonConciliar)
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Controls.Add(Me.TextBox6)
        Me.Panel1.Controls.Add(Me.ButtonCAMBIOUSUARIO)
        Me.Panel1.Controls.Add(Me.ButtonREGRESAR)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Panel6)
        Me.Panel1.Controls.Add(Me.LabelLOTE)
        Me.Panel1.Controls.Add(Me.LabelMODELO)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.ButtonACEPTAR)
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Location = New System.Drawing.Point(-1, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1493, 921)
        Me.Panel1.TabIndex = 58
        '
        'ButtonTerminarScrap
        '
        Me.ButtonTerminarScrap.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.ACEPTAR
        Me.ButtonTerminarScrap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonTerminarScrap.Enabled = False
        Me.ButtonTerminarScrap.FlatAppearance.BorderSize = 0
        Me.ButtonTerminarScrap.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonTerminarScrap.Location = New System.Drawing.Point(1083, 458)
        Me.ButtonTerminarScrap.Name = "ButtonTerminarScrap"
        Me.ButtonTerminarScrap.Size = New System.Drawing.Size(220, 73)
        Me.ButtonTerminarScrap.TabIndex = 77
        Me.ButtonTerminarScrap.UseVisualStyleBackColor = True
        Me.ButtonTerminarScrap.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.TextBoxTOTALSCRAP)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.TextBoxOUTERSCRAP)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.TextBoxINNERSCRAP)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.White
        Me.GroupBox3.Location = New System.Drawing.Point(755, 325)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(269, 205)
        Me.GroupBox3.TabIndex = 58
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "SCRAP"
        '
        'TextBoxTOTALSCRAP
        '
        Me.TextBoxTOTALSCRAP.BackColor = System.Drawing.Color.White
        Me.TextBoxTOTALSCRAP.Enabled = False
        Me.TextBoxTOTALSCRAP.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxTOTALSCRAP.Location = New System.Drawing.Point(123, 167)
        Me.TextBoxTOTALSCRAP.Name = "TextBoxTOTALSCRAP"
        Me.TextBoxTOTALSCRAP.ReadOnly = True
        Me.TextBoxTOTALSCRAP.Size = New System.Drawing.Size(119, 32)
        Me.TextBoxTOTALSCRAP.TabIndex = 37
        Me.TextBoxTOTALSCRAP.Text = "0"
        Me.TextBoxTOTALSCRAP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBoxTOTALSCRAP.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Enabled = False
        Me.Label9.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(21, 175)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(79, 24)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "TOTAL"
        Me.Label9.Visible = False
        '
        'TextBoxOUTERSCRAP
        '
        Me.TextBoxOUTERSCRAP.BackColor = System.Drawing.Color.White
        Me.TextBoxOUTERSCRAP.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxOUTERSCRAP.Location = New System.Drawing.Point(123, 102)
        Me.TextBoxOUTERSCRAP.Name = "TextBoxOUTERSCRAP"
        Me.TextBoxOUTERSCRAP.ReadOnly = True
        Me.TextBoxOUTERSCRAP.Size = New System.Drawing.Size(119, 32)
        Me.TextBoxOUTERSCRAP.TabIndex = 28
        Me.TextBoxOUTERSCRAP.Text = "0"
        Me.TextBoxOUTERSCRAP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(21, 110)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(83, 24)
        Me.Label10.TabIndex = 27
        Me.Label10.Text = "OUTER"
        '
        'TextBoxINNERSCRAP
        '
        Me.TextBoxINNERSCRAP.BackColor = System.Drawing.Color.White
        Me.TextBoxINNERSCRAP.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxINNERSCRAP.Location = New System.Drawing.Point(123, 40)
        Me.TextBoxINNERSCRAP.Name = "TextBoxINNERSCRAP"
        Me.TextBoxINNERSCRAP.ReadOnly = True
        Me.TextBoxINNERSCRAP.Size = New System.Drawing.Size(119, 32)
        Me.TextBoxINNERSCRAP.TabIndex = 26
        Me.TextBoxINNERSCRAP.Text = "0"
        Me.TextBoxINNERSCRAP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(21, 49)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(76, 24)
        Me.Label16.TabIndex = 25
        Me.Label16.Text = "INNER"
        '
        'ButtoncCODIGONOLEGIBLE
        '
        Me.ButtoncCODIGONOLEGIBLE.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.CODIGO_NO_LEGIBLE
        Me.ButtoncCODIGONOLEGIBLE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtoncCODIGONOLEGIBLE.Enabled = False
        Me.ButtoncCODIGONOLEGIBLE.FlatAppearance.BorderSize = 0
        Me.ButtoncCODIGONOLEGIBLE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtoncCODIGONOLEGIBLE.Location = New System.Drawing.Point(1069, 534)
        Me.ButtoncCODIGONOLEGIBLE.Name = "ButtoncCODIGONOLEGIBLE"
        Me.ButtoncCODIGONOLEGIBLE.Size = New System.Drawing.Size(271, 102)
        Me.ButtoncCODIGONOLEGIBLE.TabIndex = 76
        Me.ButtoncCODIGONOLEGIBLE.UseVisualStyleBackColor = True
        Me.ButtoncCODIGONOLEGIBLE.Visible = False
        '
        'TextBoxSCRAP
        '
        Me.TextBoxSCRAP.AcceptsReturn = True
        Me.TextBoxSCRAP.BackColor = System.Drawing.Color.White
        Me.TextBoxSCRAP.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxSCRAP.Location = New System.Drawing.Point(482, 566)
        Me.TextBoxSCRAP.Name = "TextBoxSCRAP"
        Me.TextBoxSCRAP.Size = New System.Drawing.Size(360, 32)
        Me.TextBoxSCRAP.TabIndex = 75
        Me.TextBoxSCRAP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBoxSCRAP.Visible = False
        '
        'TextBoxOuters
        '
        Me.TextBoxOuters.AcceptsReturn = True
        Me.TextBoxOuters.BackColor = System.Drawing.Color.White
        Me.TextBoxOuters.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxOuters.Location = New System.Drawing.Point(482, 566)
        Me.TextBoxOuters.Name = "TextBoxOuters"
        Me.TextBoxOuters.Size = New System.Drawing.Size(360, 32)
        Me.TextBoxOuters.TabIndex = 69
        Me.TextBoxOuters.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBoxOuters.Visible = False
        '
        'ButtonSCRAP
        '
        Me.ButtonSCRAP.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.SCRAP
        Me.ButtonSCRAP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonSCRAP.FlatAppearance.BorderSize = 0
        Me.ButtonSCRAP.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonSCRAP.Location = New System.Drawing.Point(19, 547)
        Me.ButtonSCRAP.Name = "ButtonSCRAP"
        Me.ButtonSCRAP.Size = New System.Drawing.Size(220, 73)
        Me.ButtonSCRAP.TabIndex = 58
        Me.ButtonSCRAP.UseVisualStyleBackColor = True
        '
        'ButtonCOMENTARIO
        '
        Me.ButtonCOMENTARIO.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.COMENTARIO
        Me.ButtonCOMENTARIO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonCOMENTARIO.FlatAppearance.BorderSize = 0
        Me.ButtonCOMENTARIO.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.ButtonCOMENTARIO.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.ButtonCOMENTARIO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonCOMENTARIO.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonCOMENTARIO.Location = New System.Drawing.Point(621, 635)
        Me.ButtonCOMENTARIO.Name = "ButtonCOMENTARIO"
        Me.ButtonCOMENTARIO.Size = New System.Drawing.Size(255, 60)
        Me.ButtonCOMENTARIO.TabIndex = 40
        Me.ButtonCOMENTARIO.UseVisualStyleBackColor = True
        '
        'ButtonSUSPENDER
        '
        Me.ButtonSUSPENDER.BackColor = System.Drawing.Color.Transparent
        Me.ButtonSUSPENDER.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.SUSPENDER
        Me.ButtonSUSPENDER.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonSUSPENDER.FlatAppearance.BorderSize = 0
        Me.ButtonSUSPENDER.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.ButtonSUSPENDER.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.ButtonSUSPENDER.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonSUSPENDER.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSUSPENDER.Location = New System.Drawing.Point(878, 553)
        Me.ButtonSUSPENDER.Name = "ButtonSUSPENDER"
        Me.ButtonSUSPENDER.Size = New System.Drawing.Size(254, 60)
        Me.ButtonSUSPENDER.TabIndex = 57
        Me.ButtonSUSPENDER.UseVisualStyleBackColor = False
        '
        'ButtonConciliar
        '
        Me.ButtonConciliar.BackColor = System.Drawing.Color.Transparent
        Me.ButtonConciliar.BackgroundImage = CType(resources.GetObject("ButtonConciliar.BackgroundImage"), System.Drawing.Image)
        Me.ButtonConciliar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonConciliar.FlatAppearance.BorderSize = 0
        Me.ButtonConciliar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.ButtonConciliar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.ButtonConciliar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonConciliar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonConciliar.Location = New System.Drawing.Point(326, 635)
        Me.ButtonConciliar.Name = "ButtonConciliar"
        Me.ButtonConciliar.Size = New System.Drawing.Size(259, 60)
        Me.ButtonConciliar.TabIndex = 39
        Me.ButtonConciliar.UseVisualStyleBackColor = False
        '
        'Panel5
        '
        Me.Panel5.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.escaner
        Me.Panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Panel5.Location = New System.Drawing.Point(236, 550)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(221, 54)
        Me.Panel5.TabIndex = 46
        '
        'TextBox6
        '
        Me.TextBox6.AcceptsReturn = True
        Me.TextBox6.BackColor = System.Drawing.Color.White
        Me.TextBox6.Enabled = False
        Me.TextBox6.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox6.Location = New System.Drawing.Point(482, 566)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(360, 32)
        Me.TextBox6.TabIndex = 45
        Me.TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ButtonCAMBIOUSUARIO
        '
        Me.ButtonCAMBIOUSUARIO.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.CAMBIO_USER
        Me.ButtonCAMBIOUSUARIO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonCAMBIOUSUARIO.FlatAppearance.BorderSize = 0
        Me.ButtonCAMBIOUSUARIO.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.ButtonCAMBIOUSUARIO.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.ButtonCAMBIOUSUARIO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonCAMBIOUSUARIO.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonCAMBIOUSUARIO.Location = New System.Drawing.Point(25, 635)
        Me.ButtonCAMBIOUSUARIO.Name = "ButtonCAMBIOUSUARIO"
        Me.ButtonCAMBIOUSUARIO.Size = New System.Drawing.Size(280, 60)
        Me.ButtonCAMBIOUSUARIO.TabIndex = 38
        Me.ButtonCAMBIOUSUARIO.UseVisualStyleBackColor = True
        '
        'ButtonREGRESAR
        '
        Me.ButtonREGRESAR.BackColor = System.Drawing.Color.Transparent
        Me.ButtonREGRESAR.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.REGRESAR
        Me.ButtonREGRESAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonREGRESAR.Enabled = False
        Me.ButtonREGRESAR.FlatAppearance.BorderSize = 0
        Me.ButtonREGRESAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonREGRESAR.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonREGRESAR.Location = New System.Drawing.Point(890, 642)
        Me.ButtonREGRESAR.Name = "ButtonREGRESAR"
        Me.ButtonREGRESAR.Size = New System.Drawing.Size(218, 43)
        Me.ButtonREGRESAR.TabIndex = 41
        Me.ButtonREGRESAR.TabStop = False
        Me.ButtonREGRESAR.UseVisualStyleBackColor = False
        Me.ButtonREGRESAR.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(43, 120)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(143, 33)
        Me.Label3.TabIndex = 68
        Me.Label3.Text = "MODELO:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(88, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 33)
        Me.Label2.TabIndex = 67
        Me.Label2.Text = "LOTE:"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.White
        Me.Panel6.Controls.Add(Me.Label21)
        Me.Panel6.Controls.Add(Me.Label22)
        Me.Panel6.Location = New System.Drawing.Point(-16, 7)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(350, 34)
        Me.Panel6.TabIndex = 66
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.Color.White
        Me.Label21.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Label21.Location = New System.Drawing.Point(31, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(123, 34)
        Me.Label21.TabIndex = 63
        Me.Label21.Text = "USUARIO:"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.Black
        Me.Label22.Location = New System.Drawing.Point(164, 5)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(88, 23)
        Me.Label22.TabIndex = 60
        Me.Label22.Text = "USUARIO"
        '
        'LabelLOTE
        '
        Me.LabelLOTE.AutoSize = True
        Me.LabelLOTE.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelLOTE.ForeColor = System.Drawing.Color.White
        Me.LabelLOTE.Location = New System.Drawing.Point(182, 69)
        Me.LabelLOTE.Name = "LabelLOTE"
        Me.LabelLOTE.Size = New System.Drawing.Size(107, 33)
        Me.LabelLOTE.TabIndex = 62
        Me.LabelLOTE.Text = "Label24"
        '
        'LabelMODELO
        '
        Me.LabelMODELO.AutoSize = True
        Me.LabelMODELO.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMODELO.ForeColor = System.Drawing.Color.White
        Me.LabelMODELO.Location = New System.Drawing.Point(182, 120)
        Me.LabelMODELO.Name = "LabelMODELO"
        Me.LabelMODELO.Size = New System.Drawing.Size(107, 33)
        Me.LabelMODELO.TabIndex = 61
        Me.LabelMODELO.Text = "Label23"
        '
        'Panel2
        '
        Me.Panel2.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.FONDO_1
        Me.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel2.Controls.Add(Me.Label15)
        Me.Panel2.Location = New System.Drawing.Point(433, 22)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(675, 282)
        Me.Panel2.TabIndex = 57
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Yellow
        Me.Label15.Location = New System.Drawing.Point(96, 47)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(496, 196)
        Me.Label15.TabIndex = 42
        Me.Label15.Text = "AQUI SE DEBEN MOSTRAR LOS MENSAJES DURANTE EL PROCESO"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.TextBoXTOTALPIEZALOTE)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.TextBoxOUTERLOTE)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.TextBoxINERLOTE)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(67, 321)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(319, 205)
        Me.GroupBox1.TabIndex = 55
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "DATOS LOTE"
        '
        'ButtonACEPTAR
        '
        Me.ButtonACEPTAR.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.ACEPTAR_1
        Me.ButtonACEPTAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonACEPTAR.FlatAppearance.BorderSize = 0
        Me.ButtonACEPTAR.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.ButtonACEPTAR.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.ButtonACEPTAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonACEPTAR.Location = New System.Drawing.Point(67, 175)
        Me.ButtonACEPTAR.Name = "ButtonACEPTAR"
        Me.ButtonACEPTAR.Size = New System.Drawing.Size(222, 76)
        Me.ButtonACEPTAR.TabIndex = 54
        Me.ButtonACEPTAR.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TextBoxPIEZASSCAN)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.TextBoxOUTERSCAN)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.TextBoxINNERSCAN)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.White
        Me.GroupBox2.Location = New System.Drawing.Point(415, 321)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(323, 205)
        Me.GroupBox2.TabIndex = 56
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "DATOS ESCANEADOS"
        '
        'frm_prod
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1364, 759)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frm_prod"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "+"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TextBoxINERLOTE As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBoxOUTERLOTE As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBoxOUTERSCAN As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextBoxINNERSCAN As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBoXTOTALPIEZALOTE As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextBoxPIEZASSCAN As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Refresh_DB_Timer As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents ButtonACEPTAR As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents LabelMODELO As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents TextBoxTOTALSCRAP As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TextBoxOUTERSCRAP As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBoxINNERSCRAP As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents LabelLOTE As System.Windows.Forms.Label
    Friend WithEvents ButtonSCRAP As System.Windows.Forms.Button
    Friend WithEvents ButtonCAMBIOUSUARIO As System.Windows.Forms.Button
    Friend WithEvents ButtonSUSPENDER As System.Windows.Forms.Button
    Friend WithEvents ButtonConciliar As System.Windows.Forms.Button
    Friend WithEvents ButtonCOMENTARIO As System.Windows.Forms.Button
    Friend WithEvents ButtonREGRESAR As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxOuters As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxSCRAP As System.Windows.Forms.TextBox
    Friend WithEvents ButtoncCODIGONOLEGIBLE As System.Windows.Forms.Button
    Friend WithEvents ButtonTerminarScrap As System.Windows.Forms.Button

End Class
